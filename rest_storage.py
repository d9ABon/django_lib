import requests
import json
import random


class RestStorage(object):
    table = None
    base_url = 'https://mrest.herokuapp.com'
    # base_url = 'http://localhost:5000'

    @classmethod
    def index(cls, page=1, results_per_page=100):
        url = "%s/%s?max_results=%d&page=%d" % (cls.base_url, cls.table, results_per_page, page)
        return requests.get(url, timeout=60).json()


    @classmethod
    def get(cls, pk):
        url = "%s/%s/%s" % (cls.base_url, cls.table, str(pk))
        res = requests.get(url, timeout=60).json()
        if '_status' in res and res['_status'] == 'ERR' or '_error' in res and res['_error'] == 404:
            return False
        return res


    @classmethod
    def find(cls, **kwargs):
        url = "%s/%s?where=%s" % (cls.base_url, cls.table, json.dumps(kwargs))
        return requests.get(url, timeout=60).json()


    @classmethod
    def find_one(cls, **kwargs):
        res = cls.find(**kwargs)
        if not len(res['_items']):
            return None
        return res['_items'][0]


    @classmethod
    def put_abstract(cls, data):
        url = "%s/%s" % (cls.base_url, cls.table)
        return requests.post(
            url,
            data = json.dumps(data),
            headers={
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            timeout=60,
        ).json()


    @classmethod
    def patch_abstract(cls, pk, data):
        item = cls.get(pk)
        if not item:
            return False
        etag = item['_etag']

        url = "%s/%s/%s" % (cls.base_url, cls.table, str(pk))
        return requests.patch(
            url,
            data = json.dumps(data),
            headers={
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'If-Match': etag,
            },
            timeout=60,
        ).json()


    @classmethod
    def delete(cls, pk_or_obj):
        if isinstance(pk_or_obj, (int, str, unicode)):
            pk = pk_or_obj
            item = cls.get(pk)
        elif '_id' in pk_or_obj:
            pk = pk_or_obj['_id']
            item = pk_or_obj
        elif '_items' in pk_or_obj:
            if len(pk_or_obj['_items']) > 1:
                raise Exception('NotImplemented: removing many objects at once')
            item = pk_or_obj['_items'][0]
            pk = item['_id']
        else:
            raise Exception(str(pk_or_obj))

        if not item:
            return False
        etag = item['_etag']

        url = "%s/%s/%s" % (cls.base_url, cls.table, str(pk))
        res = requests.delete(url, timeout=60, headers={'If-Match': etag}).content
        return bool(not len(res.strip()))


    @classmethod
    def count(cls):
        page_one = cls.index(page=1, results_per_page=1)
        return page_one['_meta']['total']


class BotIp(RestStorage):
    table = 'botip'

    @classmethod
    def put(cls, ip):
        return cls.put_abstract({
            'ip': ip.strip(),
        })


class DbUrl(RestStorage):
    table = 'db_url'

    @classmethod
    def put(cls, url, storage_max_mb, storage_used_mb=0):
        data = {
            'url': url.strip(),
            'storage_max_mb': storage_max_mb,
            'storage_used_mb': storage_used_mb,
        }
        return cls.put_abstract(data=data)


    @classmethod
    def patch(cls, pk, url=None, storage_max_mb=None, storage_used_mb=None):
        data = {'url': url, 'storage_max_mb': storage_max_mb, 'storage_used_mb': storage_used_mb}
        data = dict([(k, v) for k, v in data.items() if v is not None])  #remove None items
        return cls.patch_abstract(pk, data=data)


    @classmethod
    def get_one_url(cls):
        dbs = cls.index()['_items']
        for db in dbs:
            if db['storage_used_mb'] >= db['storage_max_mb']:
                cls.delete(db['_id'])
                del dbs[dbs.index(db)]
        if not len(dbs):
            raise Exception('no dbs left in storage')
        return random.choice(dbs)['url']


    @classmethod
    def update_storage_used_mb(cls, url, storage_used_mb):
        items = cls.find(url=url)
        if not items['_items']:
            return True  #may be already deleted
        id = items['_items'][0]['_id']
        return cls.patch(id, storage_used_mb=storage_used_mb)


class RegisteredDomain(RestStorage):
    table = 'registered_domain'

    @classmethod
    def put(cls, domain, queries_group):
        data = {
            'domain': domain,
            'queries_group': queries_group,
        }
        return cls.put_abstract(data=data)
