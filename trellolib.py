# -*- coding: utf8 -*-
import trello
import datetime
import time
import random
from collections import Counter

from base import chunks, pr

#pip install py-trello==0.6.1
#pip install pytz==2016.6.1

TRELLO_API_KEY = '50cb3a4fab6bc572a2b9c53f6a302ff1'
TRELLO_API_SECRET = 'fee2d7d7ced26ec0e8c01a686baf2f66383f406f70dab1214486af915cc6531e'
CLIENT_TOKEN = 'cceb8138eb58c73249bc779e9b7f3bfaa957dc8b1c761ba9d53d4a2b4de23d59'
CLIENT_TOKEN_SECRET = '9e26fe8ee6000e1aa09e8d8d93351301'
TRELLO_EXPIRATION = 'never'



def auth():
    from trello.util import create_oauth_token
    create_oauth_token(expiration=TRELLO_EXPIRATION, key=TRELLO_API_KEY,
                       secret=TRELLO_API_SECRET, name="trellolib",
                       scope='read,write')


def promote(promote_urls_titles):
    if not promote_urls_titles:
        raise Exception('nothing to promote for trello')

    card_links = []

    client = trello.TrelloClient(
        api_key=TRELLO_API_KEY,
        api_secret=TRELLO_API_SECRET,
        token=CLIENT_TOKEN,
        token_secret=CLIENT_TOKEN_SECRET,
    )

    boards = client.list_boards()

    board = boards[0]

    words = ' '.join([ut[1] for ut in promote_urls_titles]).split(' ')
    words = [w.decode('utf-8').lower() if not isinstance(w, unicode) else w.lower() for w in words]
    name = ' '.join([c[0] for c in Counter(words).most_common(5)])
    lst = board.add_list(name)

    for chunk in chunks(promote_urls_titles, chunk_length=4):
        words = ' '.join([ut[1] for ut in chunk]).split(' ')
        words = [w.decode('utf-8').lower() if not isinstance(w, unicode) else w.lower() for w in words]
        name = ' '.join([c[0] for c in Counter(words).most_common(5)])

        card = lst.add_card(name)

        for url, title in chunk:
            card.attach(name=title, url=url)

        if card_links:
            card.comment(random.choice(card_links)[1])
            card.comment(random.choice(card_links)[1])

        card.set_closed(True)
        card.fetch()

        card_links += [(name, card.url)]

        time.sleep(5)

    lst.close()
    return card_links


