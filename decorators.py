# try:
from functools import wraps, partial#,update_wrapper
# except ImportError:
#     from django.utils.functional import wraps#,update_wrapper    # Python 2.4 fallback.

import time
import os

from django.contrib.auth.decorators import user_passes_test#,permission_required, login_required,

#from django.core.exceptions import PermissionDenied
from django.contrib.auth import REDIRECT_FIELD_NAME

from django.shortcuts import get_object_or_404

from django.conf import settings

from django.core.exceptions import AppRegistryNotReady

from django.http import HttpResponseRedirect, HttpResponseForbidden
from django.utils.http import urlquote

from django.views.decorators.cache import cache_page as django_cache_page

import cPickle

import signal
from contextlib import contextmanager

from .base import Dummy
current_site_id = os.environ['SITE_ID'] if 'SITE_ID' in os.environ else 1
current_site = Dummy(id=1)



class SingletonException(Exception): pass
def singleton(method=None, custom_process_name=None, retries=0, sleep_interval=1.0):
    # If called without method, we've been called with optional arguments.
    # We return a decorator with the optional arguments filled in.
    # Next time round we'll be decorating method.
    if method is None:
        return partial(singleton, custom_process_name=custom_process_name, retries=retries, sleep_interval=sleep_interval)

    if not custom_process_name:
        custom_process_name = method.__name__

    @wraps(method)
    def wrapper(*args, **kwargs):
        #print 'wrapper gets', args, kwargs
        singleton_retries_count = kwargs.pop('singleton_retries_count', 0)
        if singleton_retries_count:
            print 'retrying %s, #%d' % (custom_process_name, singleton_retries_count)

        try:
            pid_file = '/tmp/%s.pid' % custom_process_name
            if os.path.exists(pid_file) and os.path.getsize(pid_file):
                with open(pid_file, 'r') as fp:
                    pid_from_file = fp.read().strip()
                cmd = "ps -p %s -o comm=" % pid_from_file
                app_name = os.popen(cmd).read().strip()
                if len(app_name) > 0:
                    raise SingletonException('Already running: %s, %s' % (custom_process_name, app_name))

        except (SingletonException, OSError) as e:  #OSError if pid file is deleted between checks
            if retries and singleton_retries_count < retries:
                time.sleep(sleep_interval)
                kwargs['singleton_retries_count'] = singleton_retries_count + 1
                return wrapper(*args, **kwargs)
            else:
                raise

        #print 'sending', args, kwargs
        with open(pid_file, 'w+') as fp:
            fp.write(str(os.getpid()))

        try:
            return method(*args, **kwargs)
        except Exception:
            raise
        finally:
            os.unlink(pid_file)

    return wrapper
"""
old singleton code includes this:
import fcntl
fp = open(pid_file, 'w')
fp.write(str(pid))
try:
    fcntl.lockf(fp, fcntl.LOCK_EX | fcntl.LOCK_NB)
except IOError:
    # another instance is running
    #sys.exit(-1)
    raise SingletonException('Already running: %s' % process_name)
"""




# def singleton_with_retry_timeout()



def deny_bots(viewfunc):
    def _decorated(request, *args, **kwargs):
        user_agent = request.META.get('HTTP_USER_AGENT', None)

        if not user_agent:
            return HttpResponseForbidden('adress removed from crawling. check robots.txt')

        bot_names = ['bot', 'spider', 'crawl', 'slurp', 'twiceler']

        for bot in bot_names:
            if bot.lower() in user_agent.lower():
                return HttpResponseForbidden('adress removed from crawling. check robots.txt')

        return viewfunc(request, *args, **kwargs)
    return _decorated

def superuser_required(function=None, login_url=None,
                                             redirect_field_name=REDIRECT_FIELD_NAME):
    actual_decorator = user_passes_test(
        lambda u: u.is_authenticated() and u.is_superuser,
        redirect_field_name=redirect_field_name
    )
    if function:
        return actual_decorator(function)
    return actual_decorator


"""
Usage:

@permission_required('blogs.change_entry')
@owner_required(Entry)
def manage_entry(request, object_id=None, object=None):

@permission_required('blogs.delete_entry')
@owner_required()
def entry_delete(*args, **kwargs):
    kwargs["post_delete_redirect"] = reverse('manage_blogs')
    return delete_object(*args, **kwargs)
"""
def owner_required(*args, **kwargs):
    kwargs['object_test'] = lambda obj: obj.user
    kwargs['user_test'] = lambda u: u
    return perm_object_and_user_based(*args, **kwargs)


def perm_object_and_user_based(object_test, user_test, model=None,
                                 kwarg_name='pk', login_url=None,
                                 redirect_field_name=REDIRECT_FIELD_NAME,
                                 replace_obj=False,allow_empty_pk=False):

    if not login_url:
        login_url = settings.LOGIN_URL

    def _decorator(viewfunc):
        def _wrapped_view(request, *args, **kwargs):
            user = request.user
            grant = False
            if kwarg_name not in kwargs:
                if not allow_empty_pk:
                    raise Exception('missing '+kwarg_name+' in kwargs')
                else:
                    #allow
                    return viewfunc(request, *args, **kwargs)

            object_id = int(kwargs[kwarg_name])

            object = get_object_or_404(model, pk=object_id)

            if user.is_superuser:
                grant = True
            else:
                if user.__class__ == model:
                    grant = object_id == user.id
                else:
                    """
                    # this selects all user's objects

                    names = [rel.get_accessor_name() for rel in user._meta.get_all_related_objects() if rel.model == model]
                    if names:
                        grant = object_id in [o.id for o in eval('user.%s.all()' % names[0])]
                    """
                    grant = object_test(object) == user_test(user)
            if not grant:

                path = urlquote(request.get_full_path())
                tup = login_url, redirect_field_name, path
                return HttpResponseRedirect('%s?%s=%s' % tup)

            if replace_obj:
                kwargs[kwarg_name] = object

            return viewfunc(request, *args, **kwargs)
        return wraps(viewfunc)(_wrapped_view)
    return _decorator

"""
def owner_required(test_func, model, kwarg_name='pk', redirect_field_name=REDIRECT_FIELD_NAME):
    @wraps(test_func)
    def view_decorator(view_function):
        @wraps(view_decorator)
        def decorated_view(request, *args, **kwargs):
            obj = get_object_or_404(model, pk=kwargs[kwarg_name])

            actual_decorator = user_passes_test(
                lambda u: (u.is_authenticated() and u.is_superuser) or u == obj.u,
                redirect_field_name=redirect_field_name
            )

            return actual_decorator(view_function)

        return decorated_view
    return view_decorator
"""



def retry_on_exception(exception, retries=3, interval=1.0, exponent=2.0):
    """A decorator to retry a function that performs db operations in case a
    exception is raised.
    :param retries:
        An integer value for the number of retries in case exception is
        raised.
    :param interval:
        A float value for the number of seconds between each interval.
    :param exponent:
        A float exponent to be applied to each retry interval.
        For example, if ``interval`` is set to 0.2 and exponent is 2.0,
        retries intervals will be in seconds: 0.2, 0.4, 0.8, etc.
    :returns:
        A decorator wrapping the target function.
    """
    def decorator(func):
        def decorated(*args, **kwargs):
            count = 0
            while True:
                try:
                    return func(*args, **kwargs)
                except exception, e:
                    print e
                    if count >= retries:
                        raise e
                    else:
                        sleep_time = (exponent ** count) * interval
                        print "Retrying function %r in %d secs" % (func, sleep_time)
                        time.sleep(sleep_time)
                        count += 1

        return decorated

    return decorator










__all__ = ["memoize"]
def memoize(function, limit=None):
    if isinstance(function, int):
        def memoize_wrapper(f):
            return memoize(f, function)

        return memoize_wrapper

    dict = {}
    list = []
    def memoize_wrapper(*args, **kwargs):
        key = cPickle.dumps((args, kwargs))
        try:
            list.append(list.pop(list.index(key)))
        except ValueError:
            dict[key] = function(*args, **kwargs)
            list.append(key)
            if limit is not None and len(list) > limit:
                del dict[list.pop(0)]

        return dict[key]

    memoize_wrapper._memoize_dict = dict
    memoize_wrapper._memoize_list = list
    memoize_wrapper._memoize_limit = limit
    memoize_wrapper._memoize_origfunc = function
    memoize_wrapper.func_name = function.func_name
    return memoize_wrapper
"""
# Example usage
if __name__ == "__main__":
    # Will cache up to 100 items, dropping the least recently used if
    # the limit is exceeded.
    @memoize(100)
    def fibo(n):
        if n > 1:
            return fibo(n - 1) + fibo(n - 2)
        else:
            return n

    # Same as above, but with no limit on cache size
    @memoize
    def fibonl(n):
        if n > 1:
            return fibo(n - 1) + fibo(n - 2)
        else:
            return n
"""


def cache_page(*args, **kwargs):
    key_prefix = kwargs.pop('key_prefix', None)
    key_prefix = "site_%d_%s" % (current_site.id, key_prefix)

    return django_cache_page(key_prefix=key_prefix, *args, **kwargs)








from functools import wraps
PROF_DATA = {}
def profile_fun(fn):
    @wraps(fn)
    def with_profiling(*args, **kwargs):
        start_time = time.time()

        ret = fn(*args, **kwargs)

        elapsed_time = time.time() - start_time

        if fn.__name__ not in PROF_DATA:
            PROF_DATA[fn.__name__] = [0, []]
        PROF_DATA[fn.__name__][0] += 1
        PROF_DATA[fn.__name__][1].append(elapsed_time)

        return ret
    return with_profiling
def print_prof_data():
    global PROF_DATA
    for fname, data in PROF_DATA.items():
        max_time = max(data[1])
        avg_time = sum(data[1]) / len(data[1])
        print "Function %s called %d times. " % (fname, data[0]),
        print 'Execution time max: %.3f, average: %.3f' % (max_time, avg_time)
    PROF_DATA = {}
"""
Usage:
@profile_fun
def your_function(...):
"""






@contextmanager
def print_time_elapsed(name):
    startTime = time.time()
    yield
    elapsedTime = time.time() - startTime
    print '[%s] finished in %.3f s' % (name, elapsedTime)






class TimeoutException(Exception):
    pass
@contextmanager
def time_limit(seconds):
    def signal_handler(signum, frame):
        raise TimeoutException, "Timed out!"
    signal.signal(signal.SIGALRM, signal_handler)
    signal.alarm(seconds)
    try:
        yield
    finally:
        signal.alarm(0)








import logging
"""
usage:
with DisableLogger():
    do_something()
"""
class DisableLogger():
    def __enter__(self):
       logging.disable(logging.CRITICAL)
    def __exit__(self, a, b, c):
       logging.disable(logging.NOTSET)


