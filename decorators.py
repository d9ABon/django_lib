from __future__ import print_function

# try:
from functools import wraps, partial  # ,update_wrapper

# except ImportError:
#     from django.utils.functional import wraps#,update_wrapper    # Python 2.4 fallback.

import time
import os
import sys

import signal
from contextlib import contextmanager

from .base import Dummy

current_site_id = os.environ['SITE_ID'] if 'SITE_ID' in os.environ else 1
current_site = Dummy(id=1)


class SingletonException(Exception):
    pass


def singleton(
    method=None, custom_process_name=None, retries=0, sleep_interval=1.0
):
    # If called without method, we've been called with optional arguments.
    # We return a decorator with the optional arguments filled in.
    # Next time round we'll be decorating method.
    if method is None:
        return partial(
            singleton,
            custom_process_name=custom_process_name,
            retries=retries,
            sleep_interval=sleep_interval,
        )

    if not custom_process_name:
        custom_process_name = method.__name__

    @wraps(method)
    def wrapper(*args, **kwargs):
        # print('wrapper gets', args, kwargs)
        singleton_retries_count = kwargs.pop('singleton_retries_count', 0)
        if singleton_retries_count:
            print(
                'retrying {!s}, #{!d}'.format(
                    custom_process_name, singleton_retries_count
                )
            )

        try:
            pid_file = '/tmp/%s.pid' % custom_process_name
            if os.path.exists(pid_file) and os.path.getsize(pid_file):
                with open(pid_file, 'r') as fp:
                    pid_from_file = fp.read().strip()
                cmd = 'ps -p %s -o comm=' % pid_from_file
                app_name = os.popen(cmd).read().strip()
                if len(app_name) > 0:
                    raise SingletonException(
                        'Already running: %s, %s'
                        % (custom_process_name, app_name)
                    )

        except (
            SingletonException,
            OSError,
        ) as e:  # OSError if pid file is deleted between checks
            if retries and singleton_retries_count < retries:
                time.sleep(sleep_interval)
                kwargs['singleton_retries_count'] = singleton_retries_count + 1
                return wrapper(*args, **kwargs)
            else:
                raise

        # print('sending', args, kwargs)
        with open(pid_file, 'w+') as fp:
            fp.write(str(os.getpid()))

        try:
            return method(*args, **kwargs)
        except Exception:
            raise
        finally:
            os.unlink(pid_file)

    return wrapper


"""
old singleton code includes this:
import fcntl
fp = open(pid_file, 'w')
fp.write(str(pid))
try:
    fcntl.lockf(fp, fcntl.LOCK_EX | fcntl.LOCK_NB)
except IOError:
    # another instance is running
    #sys.exit(-1)
    raise SingletonException('Already running: %s' % process_name)
"""


def retry_on_exception(
    exception, retries=3, interval=1.0, exponent=2.0, raise_on_fail=True
):
    """A decorator to retry a function that performs db operations in case a
    exception is raised.
    :param exception:
        Exception or list of exceptions to catch
    :param retries:
        An integer value for the number of retries in case exception is
        raised.
    :param interval:
        A float value for the number of seconds between each interval.
    :param exponent:
        A float exponent to be applied to each retry interval.
        For example, if ``interval`` is set to 0.2 and exponent is 2.0,
        retries intervals will be in seconds: 0.2, 0.4, 0.8, etc.
    :param raise_on_fail:
        Raise an exception or return False on fail
    :returns:
        A decorator wrapping the target function.
    """

    def decorator(func):
        def decorated(*args, **kwargs):
            count = 0
            while True:
                try:
                    return func(*args, **kwargs)
                except exception as e:
                    print(e)
                    if count >= retries:
                        if raise_on_fail:
                            raise e
                        else:
                            return False
                    else:
                        sleep_time = (exponent**count) * interval
                        print(
                            'Retrying function {!r} in {} secs after exception {}'.format(
                                func, sleep_time, e.__class__
                            )
                        )
                        time.sleep(sleep_time)
                        count += 1

        return decorated

    return decorator


def retry_on_empty_result(func=None, retries=3, interval=1.0, exponent=2.0):
    # If called without method, we've been called with optional arguments.
    # We return a decorator with the optional arguments filled in.
    # Next time round we'll be decorating method.
    if func is None:
        return partial(
            retry_on_empty_result,
            retries=retries,
            interval=interval,
            exponent=exponent,
        )

    @wraps(func)
    def wrapper(*args, **kwargs):
        count = 0
        while True:
            result = func(*args, **kwargs)
            if not result:
                if count >= retries:
                    return result
                else:
                    sleep_time = (exponent**count) * interval
                    print(
                        'Retrying function {!r} in {!d} secs'.format(
                            func, sleep_time
                        )
                    )
                    time.sleep(sleep_time)
                    count += 1

    return wrapper


PROF_DATA = {}


def profile_fun(fn):
    @wraps(fn)
    def with_profiling(*args, **kwargs):
        start_time = time.time()

        ret = fn(*args, **kwargs)

        elapsed_time = time.time() - start_time

        if fn.__name__ not in PROF_DATA:
            PROF_DATA[fn.__name__] = [0, []]
        PROF_DATA[fn.__name__][0] += 1
        PROF_DATA[fn.__name__][1].append(elapsed_time)

        return ret

    return with_profiling


def print_prof_data():
    global PROF_DATA
    for fname, data in PROF_DATA.items():
        max_time = max(data[1])
        avg_time = sum(data[1]) / len(data[1])
        print('Function {!s} called {!d} times. '.format(fname, data[0]))
        print(
            'Execution time max: {:.3f}, average: {:.3f}'.format(
                max_time, avg_time
            )
        )
    PROF_DATA = {}


"""
Usage:
@profile_fun
def your_function(...):
"""


@contextmanager
def print_time_elapsed(name):
    startTime = time.time()
    yield
    elapsedTime = time.time() - startTime
    print('[{!s}] finished in {:.3f} s'.format(name, elapsedTime))


class TimeoutException(Exception):
    pass


@contextmanager
def time_limit(seconds):
    def signal_handler(signum, frame):
        raise TimeoutException('Timed out!')

    signal.signal(signal.SIGALRM, signal_handler)
    signal.alarm(seconds)
    try:
        yield
    finally:
        signal.alarm(0)


def timeout(seconds):
    def decorator(func):
        def _handle_timeout(signum, frame):
            raise TimeoutException('Timed out!')

        @wraps(func)
        def wrapper(*args, **kwargs):
            signal.signal(signal.SIGALRM, _handle_timeout)
            signal.alarm(seconds)
            try:
                result = func(*args, **kwargs)
            finally:
                signal.alarm(0)
            return result

        return wrapper

    return decorator


import logging

"""
usage:
with DisableLogger():
    do_something()
"""


class DisableLogger:
    def __enter__(self):
        logging.disable(logging.CRITICAL)

    def __exit__(self, a, b, c):
        logging.disable(logging.NOTSET)


# decorater used to block function printing to the console
def block_printing_dec(func):
    def func_wrapper(*args, **kwargs):
        # block all printing to the console
        sys.stdout = open(os.devnull, 'w')
        # call the method in question
        value = func(*args, **kwargs)
        # enable all printing to the console
        sys.stdout = sys.__stdout__
        # pass the return value of the method back
        return value

    return func_wrapper


"""
usage:
with block_printing_wrp():
    do_something()
"""


class block_printing_wrp:
    def __enter__(self):
        # block all printing to the console
        sys.stdout = open(os.devnull, 'w')

    def __exit__(self, a, b, c):
        # enable all printing to the console
        sys.stdout = sys.__stdout__
        # pass the return value of the method back


class SingletonClassDecorator:
    def __init__(self, klass):
        self.klass = klass
        self.instance = None

    def __call__(self, *args, **kwds):
        if self.instance is None:
            self.instance = self.klass(*args, **kwds)
        return self.instance
