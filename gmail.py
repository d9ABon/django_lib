import sys
import imaplib
import email
import datetime


"""
if cannot login
http://www.google.com/accounts/DisplayUnlockCaptcha
"""


def dotvariants(email):
    def zerofill(integer, strlen, ch='0'):
        s = str(integer)
        return ''.join([ch for i in xrange(strlen - len(s))]) + s

    email = email[0:email.index('@')]
    variants_num = int(''.join(['1'] + ['0' for i in range(len(email) - 1)]), 2)
    for d in xrange(variants_num):
        b = "{0:b}".format(d)
        b = zerofill(b, len("{0:b}".format(variants_num)))
        generated = []
        for i, s in enumerate(email):
            if len(b) >= i + 1 and b[i] == '1':
                generated += ['.']
            generated += [email[i]]
        generated = "".join(generated)
        yield '%s@gmail.com' % generated
        yield '%s@googlemail.com' % generated


class Gmail(object):
    email = None
    password = None
    M = None
    mailbox = None

    def __init__(self, email, password, mailbox='INBOX'):
        self.email = email
        self.password = password
        self.mailbox = mailbox

        self.M = imaplib.IMAP4_SSL('imap.gmail.com')

        try:
            self.M.login(self.email, self.password)
        except imaplib.IMAP4.error:
            raise Exception("LOGIN FAILED!!! ")

        rv, mailboxes = self.M.list()
        if rv != 'OK':
            raise Exception('mailboxes not ok %s' % rv)

        rv, data = self.M.select(self.mailbox)
        if rv != 'OK':
            raise Exception('not ok selecting mailbox')

        #print "Processing mailbox...\n"


    def __del__(self):
        self.M.close()
        self.M.logout()


    def search(self, subject):
        #before was:     "SUBJECT", '"%s"' % subject
        return self.search_advanced('(SUBJECT "%s")' % subject)


    def search_advanced(self, criteria, unseen=True):
        ret = []

        rv, data = self.M.search(None, "UNSEEN" if unseen else "ALL", criteria)
        if rv != 'OK':
            print "No messages found!"
            return []

        for num in data[0].split():
            rv, data = self.M.fetch(num, '(RFC822)')
            if rv != 'OK':
                raise Exception("ERROR getting message %s" % num)

            msg = email.message_from_string(data[0][1])

            """
            print 'Message %s: %s' % (num, msg['Subject'])
            print 'Raw Date:', msg['Date']
            date_tuple = email.utils.parsedate_tz(msg['Date'])
            if date_tuple:
                local_date = datetime.datetime.fromtimestamp(
                    email.utils.mktime_tz(date_tuple))
                print "Local Date:", \
                    local_date.strftime("%a, %d %b %Y %H:%M:%S")
            """

            ret += [{
                'subject': msg['Subject'],
                'from': msg['From'],
                'to': msg['To'],
                'body': msg.get_payload(),
                'date': msg['Date'],
            }]

        return ret