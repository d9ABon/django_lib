import os
import subprocess
import json


from settings import PROJECT_PATH


class PHP:
    """This class provides a stupid simple interface to PHP code."""

    def __init__(self, prefix="", postfix=""):
        """prefix = optional prefix for all code (usually require statements)
        postfix = optional postfix for all code
        Semicolons are not added automatically, so you'll need to make sure to put them in!"""

        self.prefix = prefix
        self.postfix = postfix

    def __submit(self, code):
        p = subprocess.Popen("php", shell=False,
                             stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)

        code = code.strip().rstrip(';')

        if isinstance(self.prefix, unicode):
            self.prefix = self.prefix.encode('utf-8')
        if isinstance(self.postfix, unicode):
            self.postfix = self.postfix.encode('utf-8')
        if isinstance(code, unicode):
            code = code.encode('utf-8')

        p.stdin.write("<?php ")
        p.stdin.write(self.prefix)
        p.stdin.write('echo json_encode(' + code + ');')
        p.stdin.write(self.postfix)
        p.stdin.write(" ?>")

        out, err = p.communicate()

        p.stdin.close()
        # p.wait()

        if err:
            raise Exception('"<?php "' + self.prefix + 'echo json_encode(' + code + ');' + self.postfix + " ?>" + err)

        return out.decode('utf-8')



    def get_raw(self, code):
        """Given a code block, invoke the code and return the raw result as a string."""
        out = self.__submit(code)
        return out

    def get(self, code):
        """Given a code block that emits json, invoke the code and interpret the result as a Python value."""
        out = self.__submit(code)
        try:
            return json.loads(out)
        except ValueError as e:
            return out + "\n JSON LOAD ERROR"

    def get_one(self, code):
        """Given a code block that emits multiple json values (one per line), yield the next value."""
        out = self.__submit(code)
        for line in out:
            line = line.strip()
            if line:
                yield json.loads(line)


#php = PHP("require '../code/private/common.php';")
#code = """for ($i = 1; $i <= 10; $i++) { echo "$i\n"; }"""
#print php.get_raw(code)
#https://github.com/brool/util/blob/master/php.py

def addslashes(s):
    d = {'"':'\\"', "'":"\\'", "\0":"\\\0", "\\":"\\\\"}
    return ''.join(d.get(c, c) for c in s)


#usage: print php("test('aaa', 'bbb')", '[test.php]', {'c':'ccc'})
def php(code, php_files=None, global_vars=None):
    prefix = ''
    if php_files:
        for php_file in php_files:
            php_file = os.path.join(PROJECT_PATH, 'php', php_file)
            prefix += "require_once('%s');" % php_file

    if global_vars:
        for key, val in global_vars.items():
            if isinstance(val, (str, unicode)):
                val = addslashes(val)
                prefix += """ $%(key)s = stripslashes('%(val)s'); """ % {'key': key, 'val': val}
            else:
                val = json.dumps(val)
                prefix += """ $%(key)s = json_decode('%(val)s'); """ % {'key': key, 'val': val}

    _php = PHP(prefix=prefix)

    return _php.get(code)