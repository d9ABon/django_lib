import re
import urllib
import urllib2
import json
import random
import datetime
from base64 import b64encode, b64decode

from django.core.cache import cache


proxy_hosts = (
    'http-proxy01.appspot.com',
    'http-proxy02.appspot.com',
    'http-proxy03.appspot.com',
    'http-proxy04.appspot.com',
    'http-proxy05.appspot.com',
)

GOOGLE_TRANSLATE_DAILY_LIMIT = 90000


class TranslateLimitExceed(Exception):
    pass


def _translate(text, from_lang, to_lang, multires=False, use_proxy=False, use_limits=False):
    resp = cache.get('translate_%s_%s_%d_%s' % (from_lang, to_lang, multires, hash(text)))
    if resp:
        return resp

    current_limit = cache.get('google_translate_limit_%s' % datetime.datetime.now().strftime('%Y-%m-%d'), 0)
    if use_limits and current_limit >= GOOGLE_TRANSLATE_DAILY_LIMIT:
        raise TranslateLimitExceed('current limit: %d' % current_limit)

    """
    def print_params(data):
        #print parameters from list
        for val in data:
            if isinstance(val, basestring):
                print "     " + val
    """


    url = "http://translate.google.com/translate_a/t?%s"
    list_of_params = {'client': 't',
                      'hl': 'en',
                      'multires': '1', }

    list_of_params.update({'text': text,
                           'sl': from_lang,
                           'tl': to_lang})

    try:
        url = url % urllib.urlencode(list_of_params)
        if use_proxy:
            url = 'http://%s/proxy/%s' % (random.choice(proxy_hosts), b64encode(url))
        request = urllib2.Request(url, headers={'User-Agent': 'Mozilla/5.0', 'Accept-Charset': 'utf-8'})
        res = urllib2.urlopen(request).read()

        fixed_json = re.sub(r',{2,}', ',', res).replace(',]', ']')
        data = json.loads(fixed_json)

        resp = data[0][0][0]

        if multires:
            if isinstance(data[1], list):
                for forms in data[1]:
                    for word in forms[1]:
                        resp += ', ' + word

        #from lib.base import pr
        #pr(data)


    except Exception as e:
        print e
        #import ipdb;ipdb.set_trace()
        resp = ''

    cache.set('translate_%s_%s_%d_%s' % (from_lang, to_lang, multires, hash(text)), resp, 60 * 60)
    cache.set('google_translate_limit_%s' % datetime.datetime.now().strftime('%Y-%m-%d'), current_limit + 1, 60 * 60 * 24 * 2)

    return resp


def translate_er(text, multires=False, use_proxy=False, use_limits=False):
    return _translate(text, 'en', 'ru', multires, use_proxy)


def translate_re(text, multires=False, use_proxy=False, use_limits=False):
    return _translate(text, 'ru', 'en', multires, use_proxy, use_limits)