from django import forms

from django.utils.safestring import mark_safe
from django.forms.util import flatatt

class CakeCheckboxInput(forms.CheckboxInput):
    def render(self, name, value, attrs=None):
        final_attrs = self.build_attrs(attrs, type='checkbox',
                                                                     name=name, value=value)

        try:
            result = self.check_test(value)
        except: # Silently catch exceptions
            result = False

        if result:
            final_attrs['checked'] = 'checked'

        #if value not in ('', True, False, None):
            # Only add the 'value' attribute if a value is non-empty.
            #final_attrs['value'] = force_unicode(value)
        #else:
            #final_attrs['value'] = int(bool(value))

        final_attrs['value'] = 1
        checkbox = u'<input%s />' % flatatt(final_attrs)
        hidden = u'<input type="hidden" name="%s" value="%d" />' % (final_attrs['name'], 0)

        return mark_safe(hidden+"\n"+checkbox)


class CakeCheckboxField(forms.fields.BooleanField):
    widget = CakeCheckboxInput

    #def __init__(self, *args, **kwargs):
        #kwargs['widget'] = CakeCheckboxInput()
        #return super(CakeCheckboxField, self).__init__(*args, **kwargs)

    def clean(self, value):
        return bool(int(value))



class DateForm(forms.Form):
    day = forms.DateField()
