import os
import sys


import inspect
import traceback


from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.utils.html import escape
from django.shortcuts import render

from .base import urldecode

from cStringIO import StringIO
#devnull = open(os.devnull, 'w')
class RedirectStdStreams(object):
    def __init__(self, stdout=None, stderr=None):
        self._stdout = stdout or sys.stdout
        self._stderr = stderr or sys.stderr

    def __enter__(self):
        self.old_stdout, self.old_stderr = sys.stdout, sys.stderr
        self.old_stdout.flush(); self.old_stderr.flush()
        sys.stdout, sys.stderr = self._stdout, self._stderr

    def __exit__(self, exc_type, exc_value, traceback):
        self._stdout.flush(); self._stderr.flush()
        sys.stdout = self.old_stdout
        sys.stderr = self.old_stderr




class Api():
    commands = {}

    def get_argspec(self, command):
        argspec = inspect.getargspec(command)
        argspec = {
            'args': argspec.args if argspec.args else [],
            'defaults': argspec.defaults if argspec.defaults else [],
        }
        required_args_len = len(argspec['args']) - len(argspec['defaults'])
        argspec['defaults_dict'] = dict(zip(argspec['args'][required_args_len:], argspec['defaults']))
        return argspec, required_args_len


    def __init__(self, commands=None, module=None):
        if module and not commands:
            for name, obj in inspect.getmembers(module):
                if not inspect.isfunction(obj):
                    continue
                if inspect.getmodule(obj) != inspect.getmodule(module):
                    continue
                if name[0] == '_':
                    continue
                self.commands[name] = obj
        else:
            self.commands = commands


    def call(self, request, methodname):
        args = dict(request.POST)
        args = dict((k, urldecode(v[0])) for k,v in args.items())  #strange thing
        for k, v in args.items():
            if args[k] and args[k].isnumeric():
                args[k] = int(args[k])

        outbuffer = StringIO()
        with RedirectStdStreams(stdout=outbuffer, stderr=outbuffer):
            try:
                self.commands[methodname](**args)
            except Exception as e:
                #traceback.print_stack()
                #traceback.format_exc()
                traceback.print_exc()


        if 'text/html' in request.META['HTTP_ACCEPT']:
            resp = HttpResponse(escape(outbuffer.getvalue()).replace("\n", "\n<br />"))
        else:
            resp = HttpResponse(outbuffer.getvalue())

        resp["Access-Control-Allow-Origin"] = '*'
        resp["Access-Control-Allow-Credentials"] = "true"
        resp["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"
        resp["Access-Control-Allow-Headers"] = "Origin"

        return resp



    def help(self, request, methodname):

        helpstr = ['usage:', '/api/%s/' % methodname, 'POST arguments:']

        argspec, required_args_len = self.get_argspec(self.commands[methodname])

        for required in argspec['args'][:required_args_len]:
            helpstr += ['<%s>' % required]
        for i, optional in enumerate(argspec['args'][required_args_len:]):
            helpstr += ['[%s=%s]' % (optional, argspec['defaults'][i])]

        helpstr = "\n".join(helpstr)

        if 'text/html' in request.META['HTTP_ACCEPT']:
            resp = HttpResponse(escape(helpstr).replace("\n", "\n<br />"))
        else:
            resp = HttpResponse(helpstr)

        resp["Access-Control-Allow-Origin"] = '*'
        resp["Access-Control-Allow-Credentials"] = "true"
        resp["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"
        resp["Access-Control-Allow-Headers"] = "Origin, x_csrf_token, If-Modified-Since, If-None-Match"

        return resp


    def index(self, request):
        argspec = {}
        for methodname in self.commands.keys():
            args, required_args_len = self.get_argspec(self.commands[methodname])
            argspec[methodname] = {
                'required': args['args'][:required_args_len],
                'optional': dict(zip(args['args'][required_args_len:], args['defaults'])),
                #'optional': [{'key': k, 'value': v} for k, v in zip(args['args'][required_args_len:], args['defaults'])],
            }


        resp = render(request, './api.html', {'argspec':argspec})
        resp['Cache-Control'] = 'no-cache, max-age=0, must-revalidate, no-store'
        resp["Access-Control-Allow-Origin"] = '*'
        resp["Access-Control-Allow-Credentials"] = "true"
        resp["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"
        resp["Access-Control-Allow-Headers"] = "Origin"
        return resp
