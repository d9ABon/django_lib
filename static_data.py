# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _

ROLES = (
    ('GENERAL_USER', _('User')),
    ('MODERATOR', _('Moderator')),
    ('ADMIN', _('Admin')),
)






