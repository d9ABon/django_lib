# -*- coding: utf-8 -*-
import os, re, shutil, json
from PIL import Image
import imghdr
from lib.base import gen_passwd, transliterate_cyrilic, get_image_sizes
from django.db.models import signals
from django.template.defaultfilters import slugify
from urlparse import urlparse


def slug_get_inst_value(inst, field):
    if '.' not in field:
        return getattr(inst, field)

    field_path = field.split('.')
    value = inst
    for field in field_path:
        value = getattr(value, field)

    return value


def slug_pre_save(fields, uniq_based_on_fields=False, max_len=50):
    def pre_save(sender, **kwargs):
        inst = kwargs['instance']

        for field, slug_field in fields.items():
            value = slug_get_inst_value(inst, field)

            slug = generate_unique_slug(field, slug_field, inst, uniq_based_on_fields, max_len=max_len)
            setattr(inst, slug_field, slug)

        #import ipdb;ipdb.set_trace()

    return pre_save


def generate_unique_slug(field, slug_field, inst, uniq_based_on_fields=False, max_len=50):
    """
    Returns a slug on a name which is unique within a model's table

    This code suffers a race condition between when a unique
    slug is determined and when the object with that slug is saved.
    It's also not exactly database friendly if there is a high
    likelyhood of common slugs being attempted.

    Original pattern discussed at
    http://www.b-list.org/weblog/2006/11/02/django-tips-auto-populated-fields
    """

    value = slug_get_inst_value(inst, field)
    model = inst.__class__

    suffix = 0

    potential = base = slugify(transliterate_cyrilic(value)).replace('_', '-').replace('--', '-')[:max_len]

    if not potential: #try pk
        potential = base = str(inst.pk)

    while True:
        if suffix:
            max_len_after_suffix = max_len - len('-' + str(suffix))
            potential = potential[:max_len_after_suffix] + '-' + str(suffix)


        filter_fun = {slug_field: potential}
        if uniq_based_on_fields:
            for uniq_field in uniq_based_on_fields:
                filter_fun[uniq_field] = getattr(inst, uniq_field)


        if not model.objects.exclude(pk=inst.pk).filter(**filter_fun).count():
            return potential
        # we hit a conflicting slug, so bump the suffix & try again
        suffix += 1






class InvalidImage(Exception):
    pass

class Behavior(object):
    settings = {}
    model_class = None
    runtime = {}

    def connect(self, *args, **kwargs):
        self.settings = kwargs['settings']
        self.model_class = kwargs['model_class']
        
        #ex. set Photo.PictureBehavior to self
        setattr(self.model_class, self.__class__.__name__, self)

        signals.pre_save.connect(self.pre_save, sender=self.model_class)
        signals.post_save.connect(self.post_save, sender=self.model_class)
        signals.pre_delete.connect(self.pre_delete, sender=self.model_class)
        signals.post_delete.connect(self.post_delete, sender=self.model_class)



    def pre_save(self, sender, **kwargs):
        pass
    def post_save(self, sender, **kwargs):
        pass
    def pre_delete(self, sender, **kwargs):
        pass
    def post_delete(self, sender, **kwargs):
        pass

    def enable_callbacks(self, inst):
        beh_dict = getattr(inst, 'Behavior_callbacks_enabled', {})
        beh_dict[self.__class__.__name__] = True
        setattr(inst, 'Behavior_callbacks_enabled', beh_dict)

    def disable_callbacks(self, inst):
        beh_dict = getattr(inst, 'Behavior_callbacks_enabled', {})
        beh_dict[self.__class__.__name__] = False
        setattr(inst, 'Behavior_callbacks_enabled', beh_dict)

    def callbacks_enabled(self, inst):
        beh_dict = getattr(inst, 'Behavior_callbacks_enabled', {})

        for value in beh_dict.values():
            if not value:
                return False
        return True

    def get_uniq_ident(self, inst):
        attr_name = self.__class__.__name__+'_uniq_ident'

        #print "get_uniq_ident %s" % attr_name

        return getattr(inst, attr_name)

    def set_uniq_ident(self, inst, uniq_ident=None):
        if uniq_ident is None:
            uniq_ident = gen_passwd(10)

        attr_name = self.__class__.__name__+'_uniq_ident'

        #print inst.pk
        #print "set_uniq_ident %s %s" % (attr_name, uniq_ident)
        setattr(inst, attr_name, uniq_ident)
        return uniq_ident




class FilehostBehavior(Behavior):
    file_path_by_url = None
    remove_temp_file = True

    def connect(self, *args, **kwargs):
        self.file_path_by_url = kwargs['file_path_by_url']
        del kwargs['file_path_by_url']

        if 'remove_temp_file' in kwargs:
            self.remove_temp_file = kwargs['remove_temp_file']
            del kwargs['remove_temp_file']
        else:
            self.remove_temp_file = True

        if 'filter_instance' in kwargs:
            self.filter_instance = kwargs['filter_instance']
            del kwargs['filter_instance']



        return super(FilehostBehavior, self).connect(*args, **kwargs)

    def filter_instance(self, instance):
        return True


IMAGE_TYPES_DO_TOT_CONVERT = ['gif', 'png', 'jpeg']

class PictureBehavior(FilehostBehavior):

    def url(self, field, field_value):
        return dict([[ver_name, field_value.replace('{PREFIX}', ver_name)]
                                 for ver_name in self.settings[field]['versions'].keys()])

    def pre_save(self, sender, **kwargs):
        inst = kwargs['instance']

        if not self.callbacks_enabled(inst) or not self.filter_instance(inst):
            return True

        if inst.pk:
            prev_instance = self.model_class.objects.filter(pk=inst.pk)[0]
        else:
            prev_instance = None

        runtime = getattr(inst, 'runtime_PictureBehavior', {})

        for field in self.settings.keys():
            #if this field is not populated from cropper
            if field not in runtime or 'data_from_cropper' not in runtime[field]:
                uploaded_data = getattr(inst, field)

                if not isinstance(uploaded_data, dict):
                    continue

                try:
                    self.image_size(uploaded_data['file'])
                except (InvalidImage, KeyError):
                    if prev_instance:
                        setattr(inst, field, getattr(prev_instance, field))
                    else:
                        setattr(inst, field, None)
                else:
                    runtime[field] = uploaded_data
                    setattr(inst, field, self.generate_filename(field, inst))

                if inst.pk and prev_instance:
                    runtime[field]['old_files'] = getattr(prev_instance,
                                                    'picture_'+field)().values()


        inst.runtime_PictureBehavior = runtime



    def generate_filename(self, field, inst):
        filename = self.settings[field]['filename']

        matches = re.findall("({[A-Z_-]+})", self.settings[field]['filename'])
        for key in matches:
            if key == '{RAND}':
                filename = filename.replace(key, gen_passwd(5))
            elif key == '{PREFIX}':
                pass
            elif key == '{ID}':
                pass
            else:
                repl_field = key.lower()[1:-1]
                if getattr(inst, repl_field, None):
                    filename = filename.replace(key, str(slugify(getattr(inst, repl_field))))
        return filename



    def post_save(self, sender, **kwargs):
        inst = kwargs['instance']

#        import ipdb;ipdb.set_trace()

        if not self.callbacks_enabled(inst) or not self.filter_instance(inst):
            return True

        runtime = getattr(inst, 'runtime_PictureBehavior', {})

        for field, pre_save_data in runtime.items():
            if 'data_from_cropper' in pre_save_data:
                for dir_name in pre_save_data['data_from_cropper']['createdirs']:
                    if '{PK}' in dir_name:
                        dir_name = dir_name.replace('{PK}', str(inst.pk))

                    if not os.path.isdir(dir_name):
                        os.mkdir(dir_name, 0o777)
            
                for from_file, to_file in pre_save_data['data_from_cropper']['copyfiles'].items():
                    if '{PK}' in to_file:
                        to_file = to_file.replace('{PK}', str(inst.pk))

                    shutil.copyfile(from_file, to_file)

                for field, value in pre_save_data['data_from_cropper']['modified_fields'].items():
                    if '{PK}' in value:
                        value = value.replace('{PK}', str(inst.pk))
                    setattr(inst, field, value)
                    
            else: #regular upload
                if not pre_save_data or pre_save_data.keys() == ['old_files']:
                    continue
                field_data = getattr(inst, field)
                if '{PK}' in field_data:
                    field_data = field_data.replace('{PK}', str(inst.pk))
                    setattr(inst, field, field_data)

                modified_fields = self.save_file(field, pre_save_data, field_data)

                for field, value in modified_fields.items():
                    setattr(inst, field, value)

        self.disable_callbacks(inst)

        inst.save()

        self.enable_callbacks(inst)

        setattr(inst, 'runtime_PictureBehavior', {})



    def pre_delete(self, sender, **kwargs):
        inst = kwargs['instance']

        if not self.callbacks_enabled(inst) or not self.filter_instance(inst):
            return True

        runtime = getattr(inst, 'runtime_PictureBehavior', {})

        for field in self.settings.keys():
            runtime[field] = {'old_files': []}
            runtime[field]['old_files'] += getattr(inst, 'picture_'+field)().values()


        inst.runtime_PictureBehavior = runtime



    def post_delete(self, sender, **kwargs):
        inst = kwargs['instance']

        if not self.callbacks_enabled(inst) or not self.filter_instance(inst):
            return True

        runtime = getattr(inst, 'runtime_PictureBehavior', {})

        if not runtime: #investigate later
            return False

        for field in self.settings.keys():

            if self.settings[field].get('keep_old_files', False):
                continue

            old_files = runtime[field]['old_files']

            for ver_settings in self.settings[field]['versions'].values():

                no_pic_files = [x['no_pic_filename'] for x in ver_settings
                                                if 'no_pic_filename' in x]
                files = list(set(old_files).difference(set(no_pic_files)))
                for del_file in files:
                    del_file = self.file_path_by_url(del_file)

                    if os.path.exists(del_file):
                        os.unlink(del_file)

                    #check empty folder
                    dir_path = os.path.dirname(del_file)
                    try:
                        int(os.path.basename(dir_path)) #only numeric folders
                    except ValueError:
                        continue
                    else:
                        if os.path.exists(dir_path) and not os.listdir(dir_path):
                            os.rmdir(dir_path)

        setattr(inst, 'runtime_PictureBehavior', {})



    def save_file(self, field, pre_save_data, field_data):
#        import ipdb;ipdb.set_trace()

        filename = self.file_path_by_url(field_data) #this will be new_filename
        folder = os.path.dirname(filename)
        if not os.path.isdir(folder):
            os.mkdir(folder)

        modified_fields = {}

        for ver_name, ver_settings in self.settings[field]['versions'].items():
            new_filename = filename.replace('{PREFIX}', ver_name)
            ver_modified_fields = self.resize_version(pre_save_data, new_filename,
                                                      ver_name, ver_settings)
            modified_fields.update(ver_modified_fields)



        if not self.settings[field].get('keep_old_files', False) and 'old_files' in pre_save_data:
            #import ipdb;ipdb.set_trace();
            no_pic_filename = self.settings[field]['no_pic_filename']
            no_pic_files = [no_pic_filename.replace('{PREFIX}', ver_name)
                for ver_name in self.settings[field]['versions'].keys()]
                                            
            files = list(set(pre_save_data['old_files']).difference(set(no_pic_files)))
            for del_file in files:
                os.unlink(self.file_path_by_url(del_file))

        if self.remove_temp_file and os.path.exists(pre_save_data['file']):
            #print "remove_temp_file %s" % self.__class__.__name__
            os.unlink(pre_save_data['file'])

        return modified_fields



    def resize_version(self, current_file_info, new_filename, ver_name, ver_settings):
        current_file = current_file_info['file']

        try:
            sizes = self.image_size(current_file)
            what = imghdr.what(current_file)
        except InvalidImage:
            return False
        else:
            sizes_w, sizes_h = sizes

        max_size_if_none = lambda x: x is not None and x or 32767

        new_sizes = get_image_sizes(source_sizes=sizes,
                                     dest_sizes=(max_size_if_none(ver_settings['width']),
                                                             max_size_if_none(ver_settings['height'])),
                                     method=ver_settings['method'])

        #import ipdb;ipdb.set_trace()

        equal_size = sizes_w == new_sizes['w'] and sizes_h == new_sizes['h']
        if equal_size and what in IMAGE_TYPES_DO_TOT_CONVERT:
            #copy
            shutil.copy(current_file, new_filename)
            copied = True
        else:
            #created_image = Image.new('RGB', (new_sizes['w'], new_sizes['h']))
            image = Image.open(current_file).crop((new_sizes['crop']['x'],
                                                   new_sizes['crop']['y'],
                                                   new_sizes['crop']['x'] + new_sizes['crop']['w'],
                                                   new_sizes['crop']['y'] + new_sizes['crop']['h']))
            image = image.resize((new_sizes['w'], new_sizes['h']), Image.ANTIALIAS)

            if image.mode != "RGB":
                image = image.convert("RGB")

            image.save(new_filename, 'JPEG')
            copied = False




        modified_fields = {}

        if 'filesize_save' in ver_settings:
            size_field = ver_settings['filesize_save']
            modified_fields[size_field] = os.path.getsize(new_filename)

        if 'content_type_save' in ver_settings:
            content_type_field = ver_settings['content_type_save']
            if copied:
                modified_fields[content_type_field] = current_file_info['content_type']
            else:
                modified_fields[content_type_field] = 'image/jpeg'

        return modified_fields


    def populate_picture_data_from_cropper(self, inst, field, post_data):
        modified_fields = {field:[]}
        copyfiles = {}
        createdirs = []

#        import ipdb;ipdb.set_trace()

        runtime = getattr(inst, 'runtime_PictureBehavior', {field:{}})
        
        if 'data_from_cropper' not in runtime[field]:
            runtime[field]['data_from_cropper'] = {
                'modified_fields': {},
                'copyfiles': {},
                'createdirs': [],
            }

        if 'urls' not in post_data or not post_data['urls']:
            return False

        try:
            urls = json.loads(post_data['urls'])
        except json.JSONDecodeError:
            urls = post_data['urls']

        new_url = self.generate_filename(field, inst)

        for version, ver_settings in self.settings[field]['versions'].items():
            if 'filesize_save' in ver_settings:
                size_field = ver_settings['filesize_save']
                modified_fields[size_field] = post_data['size']
            if 'content_type_save' in ver_settings:
                content_type_field = ver_settings['content_type_save']
                modified_fields[content_type_field] = post_data['content_type']

            parsed_url = urlparse(urls[version])
            cropped_file_dir = os.path.dirname(parsed_url.path)
            final_file_dir = os.path.dirname(self.settings[field]['filename'])

            cropped_file_dir = self.file_path_by_url(cropped_file_dir)
            final_file_dir = self.file_path_by_url(final_file_dir)

            createdirs.append(final_file_dir)

            from_file = self.file_path_by_url(parsed_url.path)

            to_file = self.file_path_by_url(new_url.replace('{PREFIX}', version))

            copyfiles[from_file] = to_file

#            import ipdb;ipdb.set_trace()

            if not modified_fields[field]:
                modified_fields[field] = {version:new_url}
            else:
                modified_fields[field][version] = new_url

        modified_fields[field] = new_url

        #changes to runtime

        runtime[field]['data_from_cropper']['copyfiles'] = copyfiles
        runtime[field]['data_from_cropper']['createdirs'] = createdirs

        for key, val in modified_fields.items():
            runtime[field]['data_from_cropper']['modified_fields'][key] = val

        inst.runtime_PictureBehavior = runtime
        
        return True


    def image_size(self, uploaded_file):
        try:
            return Image.open(uploaded_file).size
        except IOError:
            raise InvalidImage()













class FileBehavior(FilehostBehavior):

    def pre_save(self, sender, **kwargs):
        inst = kwargs['instance']

        if not self.callbacks_enabled(inst) or not self.filter_instance(inst):
            return True

        if inst.pk:
            prev_instance = self.model_class.objects.filter(pk=inst.pk)[0]
        else:
            prev_instance = None

        runtime = {}

        for field in self.settings.keys():
            uploaded_data = getattr(inst, field)

            if not isinstance(uploaded_data, dict):
                continue

            if 'file' not in uploaded_data:
                if prev_instance:
                    setattr(inst, field, getattr(prev_instance, field))
                else:
                    setattr(inst, field, None)
            else:
                runtime[field] = uploaded_data
                setattr(inst, field, self.generate_filename(field, inst, uploaded_data))

            if inst.pk and prev_instance:
                runtime[field]['old_files'] = [getattr(prev_instance, field)]

        uniq_ident = self.set_uniq_ident(inst)
        self.runtime[uniq_ident] = runtime





    def post_save(self, sender, **kwargs):
        inst = kwargs['instance']

        #import ipdb;ipdb.set_trace()

        if not self.callbacks_enabled(inst) or not self.filter_instance(inst):
            return True

        uniq_ident = self.get_uniq_ident(inst)
        runtime = self.runtime[uniq_ident]

        #print self.runtime

        for field, pre_save_data in runtime.items():
            if not pre_save_data or pre_save_data.keys() == ['old_files']:
                continue
            field_data = getattr(inst, field)

            if '{PK}' in field_data:
                field_data = field_data.replace('{PK}', str(inst.pk))
                setattr(inst, field, field_data)

            modified_fields = self.save_file(field, pre_save_data, field_data)

            for field, value in modified_fields.items():
                setattr(inst, field, value)

        self.disable_callbacks(inst)

        inst.save()

        self.enable_callbacks(inst)

        del self.runtime[uniq_ident]




    def save_file(self, field, pre_save_data, field_data):
        filename = self.file_path_by_url(field_data)
        folder = os.path.dirname(filename)
        if not os.path.isdir(folder):
            os.mkdir(folder)

        current_file = pre_save_data['file']

        shutil.copy(current_file, filename)

        modified_fields = {}

        settings = self.settings[field]

        if 'filesize_save' in settings:
            size_field = settings['filesize_save']
            modified_fields[size_field] = os.path.getsize(filename)
        if 'content_type_save' in settings:
            content_type_field = settings['content_type_save']
            modified_fields[content_type_field] = pre_save_data['content_type']
        if 'filename_save' in settings:
            modified_fields[settings['filename_save']] = pre_save_data['name']

        if not self.settings[field].get('keep_old_files', False) and 'old_files' in pre_save_data:
            for del_file in pre_save_data['old_files']:
                if os.path.exists(self.file_path_by_url(del_file)):
                    os.unlink(self.file_path_by_url(del_file))

        if self.remove_temp_file and os.path.exists(pre_save_data['file']):
            #print "remove_temp_file %s" % self.__class__.__name__
            os.unlink(pre_save_data['file'])

        return modified_fields







    def pre_delete(self, sender, **kwargs):
        inst = kwargs['instance']

        if not self.callbacks_enabled(inst) or not self.filter_instance(inst):
            return True

        runtime = {}

        for field in self.settings.keys():
            runtime[field] = {'old_files': [getattr(inst, field)]}

        uniq_ident = self.set_uniq_ident(inst)
        self.runtime[uniq_ident] = runtime




    def post_delete(self, sender, **kwargs):
        inst = kwargs['instance']

        if not self.callbacks_enabled(inst) or not self.filter_instance(inst):
            return True

        for field in self.settings.keys():

            if self.settings[field].get('keep_old_files', False):
                continue

            uniq_ident = self.get_uniq_ident(inst)
            runtime = self.runtime[uniq_ident]

            old_files = runtime[field]['old_files']

            for del_file in old_files:
                del_file = self.file_path_by_url(del_file)

                if os.path.exists(del_file):
                    os.unlink(del_file)

                #check empty folder
                dir_path = os.path.dirname(del_file)
                try:
                    int(os.path.basename(dir_path)) #only numeric folders
                except ValueError:
                    continue
                else:
                    if os.path.exists(dir_path) and not os.listdir(dir_path):
                        os.rmdir(dir_path)

            del self.runtime[uniq_ident]


    def generate_filename(self, field, inst, uploaded_data):
        filename = self.settings[field]['filename']

        matches = re.findall('({[A-Z_-]+})', self.settings[field]['filename'])
        for key in matches:
            if key == '{RAND}':
                filename = filename.replace(key, gen_passwd(5))
            elif key == '{PREFIX}':
                pass
            elif key == '{ID}':
                pass
            elif key == '{EXT}':
                ext = os.path.splitext(os.path.split(uploaded_data['name'])[1])[1]
                filename = filename.replace(key, ext)
            else:
                repl_field = key.lower()[1:-1]
                if getattr(inst, repl_field, None):
                    filename = filename.replace(key,
                                        str(slugify(getattr(inst, repl_field))))
        return filename



