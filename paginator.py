import re
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator as CorePaginator

from django.http import Http404
from django.core.paginator import EmptyPage

from lib.base import combine_args_to_url


def parse_base_url(base_url):
    if not base_url:
        return None
    elif isinstance(base_url, basestring):
        #remove paginator params from base_url
        for pag_arg_name in ['page', 'sort', 'direction', 'per_page']:
            reg = re.compile('%s:([^/]+)/' % pag_arg_name)
            search_res = reg.search(base_url)

            try:
                val = search_res.groups()[0]
            except AttributeError, IndexError:
                continue
            else:
                base_url = base_url.replace('%s:%s/' % (pag_arg_name, val), '')
    else:
        #base url e.g. ('app.views.v_name', [arg], {kwarg:kwarg_val})
        #args and kwargs are not required

        #arguments for reverse function
        viewname = ''
        args = []
        kwargs = {}
        for param in base_url:
            if isinstance(param, basestring):
                viewname = param
            elif isinstance(param, list) or isinstance(param, tuple):
                args = param
            elif isinstance(param, dict):
                kwargs = param

        if 'named_params' in kwargs and isinstance(kwargs['named_params'], dict):
            #remove paginator params
            for pag_arg_name in ['page', 'sort', 'direction', 'per_page']:
                if pag_arg_name in kwargs['named_params']:
                    del kwargs['named_params'][pag_arg_name]
            kwargs['named_params'] = combine_args_to_url(kwargs['named_params'],
                urlencode_values=True)

            if not kwargs['named_params']:
                del kwargs['named_params']

        base_url = reverse(viewname=viewname, args=args, kwargs=kwargs)

        if base_url[-1] == '/': #remove last slash
            base_url = base_url[0:-1]

    return base_url

class Paginator(CorePaginator):
    
    def __init__(self, object_list, named_params=None,
                 order_by="pk", order_by_whitelist=None,
                 base_url=None, direction=None, per_page=None):

        if not named_params:
            named_params = {}
        if not order_by_whitelist:
            order_by_whitelist = []
        
        if "page" not in named_params:
            self.current_page = 1
        else:
            self.current_page = int(named_params["page"])

        if not order_by_whitelist:
            self.order_by = None
        elif "sort" in named_params and named_params["sort"] in order_by_whitelist:
            self.order_by = named_params["sort"]
        else:
            self.order_by = order_by

        if self.order_by and self.order_by[0] == '-':
            self.order_by = self.order_by[1:]
            direction = 'desc'

        if "direction" in named_params:
            self.direction = named_params["direction"]
        else:
            self.direction = direction

        if "per_page" in named_params:
            try:
                per_page = int(named_params['per_page'])
            except ValueError:
                pass


        self.base_url = parse_base_url(base_url)

        super(Paginator, self).__init__(object_list, per_page)
        
    def page(self, number = None):
        
        try:
            if self.order_by:
                if self.direction == 'desc':
                    direction = '-'
                else:
                    direction = ''
                self.object_list = self.object_list.order_by(direction+self.order_by)
        except AttributeError:
            pass
        
        if not number:
            try:
                if self.current_page:
                    try:
                        return super(Paginator, self).page(self.current_page)
                    except EmptyPage:
                        raise Http404('Empty page')
            except AttributeError:
                raise TypeError, "page number is required argument"
        return super(Paginator, self).page(number)
