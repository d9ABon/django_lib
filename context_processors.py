import datetime
import os
from django.utils.translation import to_locale, get_language

TOP_DOMAIN = os.environ['DOMAIN']
try:
    from dor.models import Site

    current_site = Site.objects.get(id=int(os.environ['SITE_ID']))

    # get TOP_DOMAIN
    parts = current_site.domain.split('.')
    if len(parts) <= 2:
        TOP_DOMAIN = current_site.domain
    elif (
        Site.objects.filter(domain__contains='.'.join(parts[-2:]))
        .exclude(id=current_site.id)
        .count()
    ):
        TOP_DOMAIN = '.'.join(parts[-2:])

except (AttributeError, IndexError):
    from lib.base import Dummy

    current_site = Dummy(id=1, domain='localhost')


def locale(request):
    return {'locale': to_locale(get_language())}


def top_domain(request):
    return {'DOMAIN': current_site.domain, 'TOP_DOMAIN': TOP_DOMAIN}


def current_time(request):
    return {'current_time': datetime.datetime.now()}


def current_url(request):
    return {'current_url': request.build_absolute_uri()}
