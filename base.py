# -*- coding: utf-8 -*-
from __future__ import print_function
from hashlib import md5
import base64
import json
import os
import sys
import tempfile, re, datetime
import collections
import random

try:
    from urllib import quote_plus, unquote_plus  # Python 2.X
except ImportError:
    from urllib.parse import quote_plus, unquote_plus  # Python 3+




class Dummy(object):
    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)



def pass_args(passed_args, urldecode_values=True):
    if not passed_args:
        return {}

    if urldecode_values:
        split_func = lambda s: (s.split(':')[0], urldecode(s.split(':')[1]))
    else:
        split_func = lambda s: s.split(':')

    try:
        passed_args = dict(map(split_func, passed_args.split('/')))
    except (ValueError, IndexError, KeyError):
        passed_args = {}

    return passed_args


def pass_args_from_full_url(url, urldecode_values=True):
    reg = re.compile('([^:]+):([^:]+)')

    parts = url.split('/')
    parts = filter(lambda x: reg.match(x), parts)
    return pass_args('/'.join(parts), urldecode_values)



def combine_args_to_url(passed_args, urlencode_values=True):
    if urlencode_values:
        join_func = lambda x: ':'.join((str(x[0]), urlencode(str(x[1]))))
    else:
        join_func = lambda x: ':'.join(map(str, x))

    #remove empty values
    filtered = {}
    filtered.update((k, v) for k, v in passed_args.iteritems() if v != '')

    filtered = map(join_func, filtered.items())
    filtered = '/'.join(filtered)

    return filtered




from hashlib import sha1
def cache_func(seconds = 900):
    from django.core.cache import cache
    """
        Cache the result of a function call for the specified number of seconds,
        using Django's caching mechanism.
        Assumes that the function never returns None (as the cache returns None to indicate a miss), and that the function's result only depends on its parameters.
        Note that the ordering of parameters is important. e.g. myFunction(x = 1, y = 2), myFunction(y = 2, x = 1), and myFunction(1,2) will each be cached separately.

        Usage:

        @cache_func(600)
        def myExpensiveMethod(parm1, parm2, parm3):
            ....
            return expensiveResult
`
    """
    def doCache(f):
        def x(*args, **kwargs):
            key = sha1(str(f.__module__) + str(f.__name__) + str(args) + str(kwargs)).hexdigest()
            result = cache.get(key)
            if result is None:
                result = f(*args, **kwargs)
                cache.set(key, result, seconds)
            return result
        return x
    return doCache



def shorthash(s):
    import base64
    import hashlib
    return base64.urlsafe_b64encode(hashlib.md5(s).digest())




def flash(request, message = None, redirect_to = None, message_type = 'notice'):
    from django.http import HttpResponse, HttpResponseRedirect
    if message:
        msg = request.session.get('flash', [])

        msg += [[message, message_type]]
        request.session['flash'] = msg

    if redirect_to:
        if redirect_to == 'referer':
            return redirect_to_referrer(request)
        return HttpResponseRedirect(redirect_to)

def redirect_to_referrer(request, except_url = '/'):
    from django.http import HttpResponse, HttpResponseRedirect
    from urlparse import urlparse
    if 'HTTP_REFERER' not in request.META:
        return HttpResponseRedirect(except_url)

    if 'referer' in request.POST:
        return HttpResponseRedirect(request.POST['referer'])
    referer = urlparse(request.META['HTTP_REFERER'])
    if request.META['PATH_INFO'] != referer.path:
        return HttpResponseRedirect(referer.path)
    else:
        return HttpResponseRedirect(except_url)

def urlencode(s):
    s = quote_plus(s)
    return s

def urldecode(s):
    s = unquote_plus(s)
    return s

"""Truncation beautifier function
This simple function attempts to intelligently truncate a given string
@author: Kelvin Wong <www.kelvinwong.ca>
@since: 2007-06-22
@version: 0.10
@license: LGPL v.2.1 http://www.gnu.org/licenses/lgpl.html
"""
def trunc(s,min_pos=0,max_pos=75,ellipsis=True):
    """Return a nicely shortened string if over a set upper limit
    (default 75 characters)

    What is nicely shortened? Consider this line from Orwell's 1984...
    0---------1---------2---------3---------4---------5---------6---------7---->
    When we are omnipotent we shall have no more need of science. There will be

    If the limit is set to 70, a hard truncation would result in...
    When we are omnipotent we shall have no more need of science. There wi...

    Truncating to the nearest space might be better...
    When we are omnipotent we shall have no more need of science. There...

    The best truncation would be...
    When we are omnipotent we shall have no more need of science...

    Therefore, the returned string will be, in priority...

    1. If the string is less than the limit, just return the whole string
    2. If the string has a period, return the string from zero to the first
            period from the right
    3. If the string has no period, return the string from zero to the first
            space
    4. If there is no space or period in the range return a hard truncation

    In all cases, the string returned will have ellipsis appended unless
    otherwise specified.

    Parameters:
            s = string to be truncated as a String
            min_pos = minimum character index to return as Integer (returned
                                string will be at least this long - default 0)
            max_pos = maximum character index to return as Integer (returned
                                string will be at most this long - default 75)
            ellipsis = returned string will have an ellipsis appended to it
                                 before it is returned if this is set as Boolean
                                 (default is True)
    Returns:
            Truncated String
    Throws:
            ValueError exception if min_pos > max_pos, indicating improper
            configuration
    Usage:
    short_string = trunc(some_long_string)
    or
    shorter_string = trunc(some_long_string,max_pos=15,ellipsis=False)
    """
    # Sentinel value -1 returned by String function rfind
    NOT_FOUND = -1
    # Error message for max smaller than min positional error
    ERR_MAXMIN = 'Minimum position cannot be greater than maximum position'

    # If the minimum position value is greater than max, throw an exception
    if max_pos < min_pos:
        raise ValueError(ERR_MAXMIN)
    # Change the ellipsis characters here if you want a true ellipsis
    if ellipsis:
        suffix = '...'
    else:
        suffix = ''
    # Case 1: Return string if it is shorter (or equal to) than the limit
    length = len(s)
    if length <= max_pos:
        return s + suffix
    else:
        # Case 2: Return it to nearest period if possible
        try:
            end = s.rindex('.',min_pos,max_pos)
        except ValueError:
            # Case 3: Return string to nearest space
            end = s.rfind(' ',min_pos,max_pos)
            if end == NOT_FOUND:
                    end = max_pos
        return s[0:end] + suffix

def month_range(year, month):
    first_day = datetime.date(year=int(year), month=int(month), day=1)

    if first_day.month == 12:
        last_day = first_day.replace(year=first_day.year + 1, month=1)
    else:
        last_day = first_day.replace(month=first_day.month + 1)
    last_day = last_day + datetime.timedelta(days=-1)
    return first_day, last_day

def gen_passwd(length=8):
    from random import choice
    import string
    chars = string.letters + string.digits
    return ''.join([choice(chars) for i in range(length)])

""" Replace in 'text' all occurences of any key in the given
dictionary by its corresponding value.    Returns the new tring."""
def multiple_replace(dct, text):
    # Create a regular expression    from the dictionary keys
    regex = re.compile("(%s)" % "|".join(map(re.escape, dct.keys())))
    # For each match, look-up corresponding value in dictionary
    return regex.sub(lambda mo: str(dct[mo.string[mo.start():mo.end()]]), text)


def handle_uploaded_file(f):
    filename = tempfile.mktemp()
    destination = open(filename, 'wb+')
    for chunk in f.chunks():
        destination.write(chunk)
    return {'file':filename, 'name':f._name, 'size': f.size,
                    'content_type': f.content_type}


def handle_with_picture_behavior(model):
    from django.http import HttpResponse, HttpResponseRedirect
    from settings import MEDIA_ROOT, MEDIA_URL

    def handler(request, form, original):
        tmp_dir_path = os.path.join(os.path.abspath(MEDIA_ROOT), 'tmp')
        tmp_dir_url = MEDIA_URL + 'tmp/'

        handle, tmpfilepath = tempfile.mkstemp(suffix='', prefix='{PREFIX}_',
                                               dir=tmp_dir_path)

        remove_temp_file = model.PictureBehavior.remove_temp_file
        model.PictureBehavior.remove_temp_file = False

        data = {
            'file':original.image.path,
            'content_type':get_content_type(original.image.path),
        }

        modified_fields = model.PictureBehavior.save_file(field='file_url',
                                        pre_save_data=data,
                                        field_data=tmpfilepath)

        model.PictureBehavior.remove_temp_file = remove_temp_file

        url = model.PictureBehavior.url('file_url',
                                    tmp_dir_url + os.path.basename(tmpfilepath))

        ret = {'result':'success',
               'orig_name':original.image.name,
               'orig_path':original.image.path,
               'orig_url':original.image.url,
               'fields':modified_fields,
               'url':url,}

        return HttpResponse(json.dumps(ret))

    return handler


def form_errors_to_json(request, form, *args, **kwards):
    from django.http import HttpResponse, HttpResponseRedirect
    return HttpResponse(json.dumps({'result':'error', 'errors':form.errors,}))


def get_content_type(filepath):
    try:
        import magic
    except ImportError as e:
        if os.name == 'nt':
            return '' #test server

        raise ImportError("\n".join([e.message, 'uses python-magic', 'git clone https://github.com/ahupp/python-magic.git && cd python-magic && python setup.py install']))

    mime = magic.Magic(mime=True)
    return mime.from_file(filepath)



def is_ajax(request):
    get_param = request.GET.get('call') and request.GET['call'] == 'ajax'
    return request.is_ajax() or get_param



"""
@see http://djangosnippets.org/snippets/499/
"""
class lazy_string(object):
    def __init__(self, function, *args, **kwargs):
        self.function=function
        self.args=args
        self.kwargs=kwargs

    def __str__(self):
        if not hasattr(self, 'str'):
            self.str=self.function(*self.args, **self.kwargs)
        return self.str

    def __getstate__(self): return self.__dict__
    def __setstate__(self, d): self.__dict__.update(d)


class UrlManipulate(object):
    url = None

    def __init__(self, url):
        self.url = url

    def __unicode__(self):
        return self.url


    def get_named_param(self, name):
        try:
            return re.search('/%s:([-_\w]+)/' % name, self.url).groups()[0]
        except (IndexError, AttributeError):
            return None

    def set_named_param(self, name, value):
        current_val = self.get_named_param(name)

        if current_val:
            self.url = self.url.replace('/%s:%s/' % (name, current_val),
                                        '/%s:%s/' % (name, value))
            return self

        #adding

        if '?' in self.url:
            index = self.url.index('?')
        elif '#' in self.url:
            index = self.url.index('#')
        else:
            index = len(self.url)

        self.url = '%(before)s%(name)s:%(value)s/%(after)s' % {
            'before': self.url[:index],
            'name': name,
            'value': str(value),
            'after': self.url[index:],
        }

        return self





class DictDiffer(object):
    """
    Calculate the difference between two dictionaries as:
    (1) items added
    (2) items removed
    (3) keys same in both but changed values
    (4) keys same in both and unchanged values
    """
    def __init__(self, past_dict, current_dict):
        self.current_dict, self.past_dict = current_dict, past_dict
        self.set_current, self.set_past = set(current_dict.keys()), set(past_dict.keys())
        self.intersect = self.set_current.intersection(self.set_past)
    def added(self):
        return self.set_current - self.intersect
    def removed(self):
        return self.set_past - self.intersect
    def changed(self):
        return set(o for o in self.intersect if self.past_dict[o] != self.current_dict[o])
    def unchanged(self):
        return set(o for o in self.intersect if self.past_dict[o] == self.current_dict[o])


class FrozenDict(collections.Mapping):
    def __init__(self, *args, **kwargs):
        self._d = dict(*args, **kwargs)
        self._hash = None
    def __iter__(self):
        return iter(self._d)
    def __len__(self):
        return len(self._d)
    def __getitem__(self, key):
        return self._d[key]
    def __hash__(self):
        # It would have been simpler and maybe more obvious to
        # use hash(tuple(sorted(self._d.iteritems()))) from this discussion
        # so far, but this solution is O(n). I don't know what kind of
        # n we are going to run into, but sometimes it's hard to resist the
        # urge to optimize when it will gain improved algorithmic performance.
        if self._hash is None:
            self._hash = 0
            for key, value in self.iteritems():
                self._hash ^= hash(key)
                self._hash ^= hash(value)
        return self._hash


"""
from HTMLParser import HTMLParser
def strip_tags(html):
    result = []
    parser = HTMLParser()
    parser.handle_data = result.append
    parser.feed(html)
    parser.close()
    return ''.join(result)
"""



def htmlspecialchars_decode_func(m, defs=None):
    if defs is None:
        import htmlentitydefs
        defs = htmlentitydefs.entitydefs
    try:
        return defs[m.group(1)]
    except KeyError:
        return m.group(0) # use as is

def htmlspecialchars_decode(string):
    pattern = re.compile("&(\w+?);")
    return pattern.sub(htmlspecialchars_decode_func, string)

#pretty printer
def pr(data, *args, **kwargs):
    # pp = pprint.PrettyPrinter(indent=indent, depth=depth)
    import pprint

    class MyPrettyPrinter(pprint.PrettyPrinter):
        def format(self, obj, context, maxlevels, level):
            if sys.version_info[0] >= 3:  #python3
                if isinstance(obj, str):
                    return obj, True, False
            else:  #python2
                if isinstance(obj, unicode):
                    return obj.encode('utf-8').decode('utf-8'), True, False
                    # return obj.encode('utf-8'), True, False
                elif isinstance(obj, str):
                    try:
                        return obj, True, False
                    except Exception:
                        pass
            return pprint.PrettyPrinter.format(self, obj, context, maxlevels, level)

    width = kwargs.pop('width', 160)

    try:
        return MyPrettyPrinter(width=width, *args, **kwargs).pprint(data)
    except UnicodeDecodeError as e:
        print(e)
        return None

#simplejson.dumps class instances
def make_serializable_deep(data):
    if isinstance(data, dict):
        for k, v in data.items():
            data[k] = make_serializable_deep(v)
        return data
    elif isinstance(data, list):
        for k, v in enumerate(data):
            data[k] = make_serializable_deep(v)
        return data
    elif isinstance(data, tuple):
        new_data = []
        for v in data:
            new_data += [make_serializable_deep(v)]
        return tuple(new_data)
    elif isinstance(data, (str, int, float, datetime.datetime)):
        return data
    else:
        return str(data)


def json_loads_recursive(data):
    if isinstance(data, (str, unicode)):
        try:
            data = json.loads(data)
        except ValueError as e:  #already loaded
            return data

    if isinstance(data, dict):
        ret = {}
        for k, v in data.items():
            ret[k] = json_loads_recursive(v)
        return ret
    elif isinstance(data, list):
        return [json_loads_recursive(v) for v in data]
    elif isinstance(data, tuple):
        return (json_loads_recursive(v) for v in data)
    else:
        raise Exception('json_loads_recursive: unknown type. not implemented yet?')



def less_relevant_search(model, search_term, results_count, exclude_ids=None):
    from django.core.cache import cache
    if not exclude_ids:
        exclude_ids = []

    cache_key = "%s_%s_%d_%s" % (model.__name__, search_term, results_count, ','.join(map(str, exclude_ids)))
    cache_key = md5(cache_key.encode('utf-8')).hexdigest()
    results = cache.get(cache_key, [])
    if results:
        return model.objects.filter(id__in=results)

    #search_term = search_term.encode('utf-8')
    search_term = search_term.split(' ')

        
    search_term = filter(lambda s: len(s) > 3, search_term) #remove short words

    #try to remove every word in search
    query = search_term
    while len(results) < results_count and len(query) > 0:
        try:
            qs = model.search(' '.join(query))
            exclude = exclude_ids + [res.id for res in results]
            tmpresults = qs[0:results_count * 2]
            tmpresults_ids = [res.id for res in tmpresults]
            for id in exclude: #exclude from tmpresults
                if id in tmpresults_ids:
                    del tmpresults[tmpresults_ids.index(id)]
            #print(' '.join(query), exclude, tmpresults)
        except Exception as e:
            print(e)
            break
        else:
            random.shuffle(tmpresults)
            results += tmpresults[:results_count]

            random.shuffle(query)
            query = query[:-1] #remove one word

    #try to search each word in query
    query = search_term
    for word in query:
        if len(results) >= results_count:
            break

        try:
            qs = model.search(word)
            tmpresults = qs[0:results_count * 2]
            tmpresults_ids = [res.id for res in tmpresults]
            exclude = exclude_ids + [res.id for res in results]
            for obj_id in exclude: #exclude from tmpresults
                if obj_id in tmpresults_ids:
                    del tmpresults[tmpresults_ids.index(obj_id)]
            #print(word, exclude, tmpresults)
        except Exception as e:
            print(e)
            break
        else:
            results += tmpresults[:results_count]

    results = results[:4] #bug?

    cache.set(cache_key, [result.id for result in results], 60*60*24)

    return results

def send_email_to_admins(template, context, subject):
    from django.core.mail import EmailMultiAlternatives
    from django.template import Context, loader


    from django.contrib.sites.models import get_current_site
    try:
        current_site = get_current_site(None)
    except AttributeError:
        from lib.base import Dummy
        current_site = Dummy(id=1, domain=os.environ['DOMAIN'])


    import settings
    t = loader.get_template(template)
    c = {
         'domain': current_site.domain,
    }
    c.update(context)
    to = ["%s<%s>" % name_and_email for name_and_email in settings.MANAGERS]

    subject = text_content = subject % c
    html_content = t.render(Context(c))

    msg = EmailMultiAlternatives(subject, text_content, None, to)
    msg.attach_alternative(html_content, "text/html")
    msg.send()


def transliterate_cyrilic(s):
    iso = {
        "а":"a","б":"b","в":"v","г":"g","д":"d",
        "е":"e","ё":"yo","ж":"zh",
        "з":"z","и":"i","й":"j","к":"k","л":"l",
        "м":"m","н":"n","о":"o","п":"p","р":"r",
        "с":"s","т":"t","у":"u","ф":"f","х":"h",  #"х":"x",
        "ц":"c","ч":"ch","ш":"sh","щ":"shh","ъ":"",
        "ы":"y","ь":"","э":"e","ю":"yu","я":"ya",
        "А":"a","Б":"b","В":"v","Г":"g","Д":"d",
        "Е":"e","Ё":"yo","Ж":"zh",
        "З":"z","И":"i","Й":"j","К":"k","Л":"l",
        "М":"m","Н":"n","О":"o","П":"p","Р":"r",
        "С":"s","Т":"t","У":"u","Ф":"f","Х":"h",  #"Х":"x",
        "Ц":"c","Ч":"ch","Ш":"sh","Щ":"shh","Ъ":"",
        "Ы":"y","Ь":"","Э":"e","Ю":"yu","Я":"ya",
    }
    iso2 = {
        u"а":"a",u"б":"b",u"в":"v",u"г":"g",u"д":"d",
        u"е":"e",u"ё":"yo",u"ж":"zh",
        u"з":"z",u"и":"i",u"й":"j",u"к":"k",u"л":"l",
        u"м":"m",u"н":"n",u"о":"o",u"п":"p",u"р":"r",
        u"с":"s",u"т":"t",u"у":"u",u"ф":"f",u"х":"h",  #u"х":"x",
        u"ц":"c",u"ч":"ch",u"ш":"sh",u"щ":"shh",u"ъ":"",
        u"ы":"y",u"ь":"",u"э":"e",u"ю":"yu",u"я":"ya",
        u"А":"A",u"Б":"B",u"В":"V",u"Г":"G",u"Д":"D",
        u"Е":"E",u"Ё":"YO",u"Ж":"ZH",
        u"З":"Z",u"И":"I",u"Й":"J",u"К":"K",u"Л":"L",
        u"М":"M",u"Н":"N",u"О":"O",u"П":"P",u"Р":"R",
        u"С":"S",u"Т":"T",u"У":"U",u"Ф":"F",u"Х":"H",  #u"Х":"X",
        u"Ц":"C",u"Ч":"CH",u"Ш":"SH",u"Щ":"SHH",u"Ъ":"",
        u"Ы":"Y",u"Ь":"",u"Э":"E",u"Ю":"YU",u"Я":"YA",
    }
#    iso2 = {}
#    for k, v in iso.items():
#        iso2[unicode(k)] = v

    pattern = re.compile('|'.join(iso.keys()))
    try:
        s = pattern.sub(lambda x: iso[x.group()], s)
    except Exception:
        s = gen_passwd(6)


    pattern = re.compile('|'.join(iso2.keys()))
    try:
        s = pattern.sub(lambda x: iso2[x.group()], s)
    except Exception:
        s = gen_passwd(6)

    return s

def slugify(value):
    from django.template.defaultfilters import slugify as django_slugify
    return django_slugify(transliterate_cyrilic(value)).replace('_', '-').replace('--', '-')



def cols(*cols, **kwargs):
    import texttable

    def space_if_none(s):
        if s is None:
            return u' '
        return s

    table = texttable.Texttable()
    table.set_deco(texttable.Texttable.HEADER)
    table.set_cols_align(["l" for i in range(len(cols))])

    widths = kwargs.get('widths')
    if widths:
        force_widths = True
    else:
        force_widths = False
        widths = [17 for i in range(len(cols))]

    rows = []
    for row_num in range(len(cols[0])):
        rows += [[]]
        for col_num, col in enumerate(cols):
            val = space_if_none(col[row_num]).encode('utf-8')

            if len(val.decode("utf-8")) > widths[col_num]:
                if not force_widths:
                    widths[col_num] = 40
            #else:
            #    val = val[:widths[col_num]]
            rows[row_num] += [val]
        table.add_rows([rows[row_num]], header=False)

    table.set_cols_width(widths)

    #table.add_rows([rows], header=False)
    return table.draw()

def split_to_cols(data):
    #ret = [[]] * len(data[0])
    ret = [[] for k in range(len(data[0]))]
    for row_num, row in enumerate(data):
        for i, val in enumerate(row):
            ret[i] += [val]
    return ret



def uniq(seq):
    seen = set()
    seen_add = seen.add
    return [ x for x in seq if x not in seen and not seen_add(x)]


def flattern_list(list_of_lists):
    return [item for sublist in list_of_lists for item in sublist]


def get_stopwords(lang):
    from settings import PROJECT_PATH
    from django.core.cache import cache
    stopwords = cache.get('stopwords_%s' % lang)
    if stopwords is not None:
        return stopwords
    f = open('%s/stopwords_%s.txt' % (PROJECT_PATH, lang), 'r')
    stopwords = f.readlines()
    stopwords = [w.strip() for w in stopwords]
    f.close()

    cache.set('stopwords_%s' % lang, stopwords, 60*60*24)

    return stopwords


def prcolored(*args):
    from termcolor import colored
    COLORS = ['red', 'green', 'yellow', 'blue', 'magenta', 'cyan', 'white']
    return ' '.join([colored(s, COLORS[hash(s) % len(COLORS)]) for s in args])





def encrypt(key, clear):
    enc = []
    for i in range(len(clear)):
        key_c = key[i % len(key)]
        enc_c = chr((ord(clear[i]) + ord(key_c)) % 256)
        enc.append(enc_c)
    return base64.urlsafe_b64encode("".join(enc))

def decrypt(key, enc):
    enc = str(enc)
    dec = []
    enc = base64.urlsafe_b64decode(enc)
    for i in range(len(enc)):
        key_c = key[i % len(key)]
        dec_c = chr((256 + ord(enc[i]) - ord(key_c)) % 256)
        dec.append(dec_c)
    return "".join(dec)


"""
Breaks a list up into a list of lists of size <chunk_length>, or list of <chunks_count> length
"""
def chunks(lst, chunk_length=None, chunks_count=None):
    if not len(lst):
        return []

    if sys.version_info[0] >= 3:  #python3
        xrange = range

    if chunk_length:
        import itertools
        clen = int(chunk_length)
        i = iter(lst)
        while True:
            chunk = list(itertools.islice(i, clen))
            if chunk:
                yield chunk
            else:
                break
    elif chunks_count:
        chunks_count = min(chunks_count, len(lst))
        chunksize, remainder = divmod(len(lst), chunks_count)
        sizes = [chunksize] * chunks_count
        if remainder:
            sizes[-remainder:] = [chunksize+1 for x in xrange(remainder)]
        idx = 0
        for s in sizes:
            yield lst[idx:idx+s]
            idx += s


def sentences(text):
    text = re.sub(r'\w+:\/{2}[\d\w-]+(\.[\d\w-]+)*(?:(?:\/[^\s/]*))*', '', text, flags=re.M|re.S)  #remove urls
    text = re.sub(r'\.[\s\.]+', '. ', text, flags=re.M|re.S)
    text = re.sub(r'\s+\.', '. ', text, flags=re.M|re.S)
    text = re.sub(r'\s+', ' ', text, flags=re.M|re.S)
    splitted = re.split(r'([\.!?\n]+\s+)', text.strip())  #'sent1. sent2? ...' -> ['sent1', '. ', 'sent2', '? ', ...]
    every_other_joined = list(zip(splitted[0::2], splitted[1::2]))  #[1,2,3,4,5,6] -> [(1, 2), (3, 4), (5, 6)]
    if len(splitted) % 2 == 1:
        every_other_joined += [(splitted[-1],)]
    sentences = [''.join(sent) for sent in every_other_joined]
    sentences = [s.strip() for s in sentences]
    # for sent in sentences:
    #     print(sent)
    #     print('----------')
    # print(" ".join(sentences))
    return sentences


def strtr(strng, replace):
    if replace and strng:
        s, r = replace.popitem()
        return r.join(strtr(subs, dict(replace)) for subs in strng.split(s))
    return strng



def get_auto_increment_for_model(model):
    from django.db import connection
    from settings import DATABASES

    sql = """
        SELECT `AUTO_INCREMENT`
        FROM  INFORMATION_SCHEMA.TABLES
        WHERE TABLE_SCHEMA = '%s'
        AND   TABLE_NAME   = '%s';
    """ % (DATABASES['default']['NAME'], model._meta.db_table)
    cursor = connection.cursor()
    cursor.execute(sql)
    return cursor.fetchone()[0]


def solve_captcha(url, ru=True):
    from lib.antigate import AntiGate
    import urllib
    import tempfile
    import os
    from settings import PROJECT_PATH, global_settings

    if url[:2] == '//':
        url = 'http:%s' % url

    captcha_file = tempfile.mktemp(prefix='captcha_', dir='/tmp/')
    urllib.urlretrieve(url, captcha_file)
    gate = AntiGate(global_settings()['antigate_api_key'], captcha_file, send_config={'is_russian': '1', 'numeric': '2'})
    captcha = str(gate)
    os.unlink(captcha_file)
    print(captcha)
    return captcha, gate


def termbold(s):
    ANSI_BOLD = u'\033[1m'
    ANSI_NORMAL = u'\033[22m'
    def bold(text):
        return ''.join([ANSI_BOLD, text, ANSI_NORMAL])
    print(bold(s))


def termtitle(s):
    ANSI_WINDOW_NAME_START = u'\033]2;'
    ANSI_WINDOW_NAME_END = u'\007'
    def title(text):
        return ''.join([ANSI_WINDOW_NAME_START, text, ANSI_WINDOW_NAME_END])
    print(title(s))


def instapush(s):
    import requests

    data = {
        "event": "message",
        "trackers": {
            "message": s,
        },
    }
    headers = {
        'x-instapush-appid': '5771f5225659e3e87db46fd2',
        'x-instapush-appsecret': '521eb2ee3a34df5501dad56e083451fb',
        'Content-Type': 'application/json',
    }
    url = 'https://api.instapush.im/v1/post'

    res = requests.post(url, data=json.dumps(data), headers=headers)
    return res.json()




def get_image_sizes(source_sizes, dest_sizes, method, allow_zoom_in = False,
                                        allow_zoom_out = True, proportions = None):

    source_w, source_h = map(float, source_sizes)
    dest_w, dest_h = map(float, dest_sizes)

    """
    @note: not used. maybe needs rewriting
    """
    if proportions:
        max_size = max(dest_w, dest_h)
        return int(max_size * float(proportions)), int(max_size)

    if method == 'crop':
        sizes = {'coeff': {'w': source_w / dest_w, 'h': source_h / dest_h}}

        if sizes['coeff']['w'] > sizes['coeff']['h']: #crop from left and right
            sizes['crop'] = {'w':source_h * (dest_w / dest_h),
                                             'h':source_h}
        else: #crop trom top and bottom
            sizes['crop'] = {'w':source_w,
                                             'h':source_w * (dest_h / dest_w)}

        sizes['crop']['x'] = (source_w - sizes['crop']['w']) / 2
        sizes['crop']['y'] = (source_h - sizes['crop']['h']) / 2
        return {'w':int(round(dest_w)),
                'h':int(round(dest_h)),
                'crop':{
                    'x':int(round(sizes['crop']['x'])),
                    'y':int(round(sizes['crop']['y'])),
                    'w':int(round(sizes['crop']['w'])),
                    'h':int(round(sizes['crop']['h']))}}

    #resize
    zoom_out = not allow_zoom_in and (source_w > dest_w or source_h > dest_h)
    zoom_in = not allow_zoom_out and (source_w < dest_w and source_h < dest_h)
    if zoom_out or zoom_in:
        if (source_w / dest_w) > (source_h / dest_h):
            new_w = dest_w
            new_h = source_h * (dest_w / source_w)
        else:
            new_w = source_w * (dest_h / source_h)
            new_h = dest_h
    else:
        new_w = source_w
        new_h = source_h

    return {'w': int(round(new_w)),
            'h': int(round(new_h)),
            'crop': {
                 'x':0,
                 'y':0,
                 'w':int(round(source_w)),
                 'h':int(round(source_h))}}



