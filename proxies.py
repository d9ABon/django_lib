#!/bin/env python
#require python-binary-memcached http-request-randomizer
#account at redislabs.com: iuyuukukiu@gmail.com DB:proxies

import bmemcached
import time
import random

ENDPOINT = "memcached-18888.c81.us-east-1-2.ec2.cloud.redislabs.com:18888"
USERNAME = 'mc-h7jqC'
PASSWORD = 'G1ICdIX8wKrlIbXvdcxFzMH2sWkQvyC5'

import logging
logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

from http_request_randomizer.requests.proxy.requestProxy import RequestProxy
logging.getLogger("http_request_randomizer").setLevel(logging.WARNING)


from .decorators import SingletonClassDecorator

@SingletonClassDecorator
class McConnector(object):
    connections = {}
    def connect(self, endpoints, username, password):
        return bmemcached.Client(endpoints, username, password)
    def __call__(self, endpoints, username, password):
        h = hash(repr((endpoints, username, password)))
        if h not in self.connections:
            self.connections[h] = self.connect(endpoints, username, password)
        return self.connections[h]



class Proxies(object):
    mc = None
    listname = None
    req_proxy = None

    def __init__(self, listname='default'):
        self.listname = listname
        self.mc = McConnector()([ENDPOINT], USERNAME, PASSWORD)


    def get_new_proxies(self):
        while self.mc.get('lock'):
            time.sleep(2)

        recent = self.mc.get('recent_proxies')
        if recent:
            random.shuffle(recent)
            self.mc.set('list_{}'.format(self.listname), recent, 60*60)
            self.randomize_proxy()
            return

        self.mc.set('lock', True, 60)
        self.req_proxy = RequestProxy(sustain=True)
        self.req_proxy.set_logger_level(logging.WARNING)
        # self.req_proxy.logger.removeHandler(self.req_proxy.logger.handlers[-1])

        proxies = [(p.get_address(), [pr.name for pr in p.protocols])
                        for p in self.req_proxy.get_proxy_list()]

        #seems like phantomjs only allows HTTP and SOCKS5
        proxies = [p[0] for p in proxies if 'HTTP' in p[1]
                                            or 'SOCS5' in p[1]]

        random.shuffle(proxies)
        self.mc.set('list_{}'.format(self.listname), proxies, 60*60)
        self.mc.set('recent_proxies', proxies, 60*60)
        self.randomize_proxy()
        self.mc.set('lock', False, 60)


    def randomize_proxy(self):
        proxies = self.mc.get('list_{}'.format(self.listname))

        if not proxies or not len(proxies):
            self.get_new_proxies()
            proxies = self.mc.get('list_{}'.format(self.listname))

        current = proxies.pop()
        self.mc.set('current_{}'.format(self.listname), current, 60*60*6)
        self.mc.set('list_{}'.format(self.listname), proxies, 60*60*6)

        logger.info('swithing to proxy {}'.format(current))


    def current(self):
        current = self.mc.get('current_{}'.format(self.listname))
        if not current:
            self.get_new_proxies()
        return self.mc.get('current_{}'.format(self.listname))


if __name__ == '__main__':
    proxies = Proxies(1)

    print('--- 1 ---')
    print(proxies.current())
    proxies.randomize_proxy()
    print(proxies.current())

    print('--- 2 ---')
    proxies2 = Proxies(2)
    print(proxies2.current())
    print('--- 3 ---')
    proxies3 = Proxies(3)
    print(proxies3.current())
