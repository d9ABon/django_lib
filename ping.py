import requests
import re
from base import urlencode

def yandex(title, url):
    if isinstance(title, unicode):
        title = title.encode('utf-8')
    if isinstance(url, unicode):
        title = url.encode('utf-8')

    request_body = b"""<?xml version="1.0" encoding="UTF-8"?>\n<methodCall>\n<methodName>weblogUpdates.ping</methodName>\n<params>\n<param>\n<value>%s</value>\n</param>\n<param>\n<value>%s</value>\n</param>\n</params>\n</methodCall>\n""" % (
        title,
        url,
    )

    print requests.post('http://ping.blogs.yandex.ru/RPC2', request_body, headers={
        'POST': '/',
        'Content-Type': 'text/xml',
        'User-Agent': 'Python-xmlrpc/3.5',
        'Content-Length': str(len(request_body)),
    }).content


def google(sitemap_url):
    print requests.get("https://www.google.com/webmasters/tools/ping?sitemap=%s" % urlencode(sitemap_url)).content
    domain = re.search('https?://([^/]+)/.*', sitemap_url).groups()[0]
    print requests.get("http://blogsearch.google.ru/ping?url=%s&hl=ru" % urlencode(domain)).content


def bing(sitemap_url):
    print requests.get("http://www.bing.com/webmaster/ping.aspx?siteMap=%s" % urlencode(sitemap_url)).content
