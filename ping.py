import requests
import re
import urllib
import json
import random
from base import urlencode
from decorators import time_limit, TimeoutException

def yandex(title, url, requester=False):
    return False  #DEPRECATED

    if isinstance(title, unicode):
        title = title.encode('utf-8')
    if isinstance(url, unicode):
        url = url.encode('utf-8')

    request_body = b"""<?xml version="1.0" encoding="UTF-8"?>\n<methodCall>\n<methodName>weblogUpdates.ping</methodName>\n<params>\n<param>\n<value>%s</value>\n</param>\n<param>\n<value>%s</value>\n</param>\n</params>\n</methodCall>\n""" % (
        title,
        url,
    )

    headers = {
        'POST': '/',
        'Content-Type': 'text/xml',
        'User-Agent': 'Python-xmlrpc/3.5',
        'Content-Length': str(len(request_body)),
    }

    try:
        with time_limit(10):
            if not requester:
                print requests.post('http://ping.blogs.yandex.ru/RPC2', request_body, headers=headers).content
            else:
                url = "%srequest/?url=%s&custom_http_headers=%s" % (
                    requester,
                    urllib.quote('http://ping.blogs.yandex.ru/RPC2', safe=''),
                    urllib.quote(json.dumps(headers), safe='')
                )
                print requests.post(url, request_body).content
    except TimeoutException as e:
        print 'seems like ping.blogs.yandex.ru is not working'


def google(sitemap_url, requester=False):
    domain = re.search('https?://([^/]+)/.*', sitemap_url).groups()[0]

    url1 = "https://www.google.com/webmasters/tools/ping?sitemap=%s" % urlencode(sitemap_url)
    url2 = "http://blogsearch.google.ru/ping?url=%s&hl=ru" % urlencode(domain)
    url3 = "http://blogsearch.google.com/ping?url=%s&hl=ru" % urlencode(domain)
    if requester:
       url1 = "%srequest/?url=%s" % (requester, urllib.quote(url1, safe=''))
       url2 = "%srequest/?url=%s" % (requester, urllib.quote(url2, safe=''))
       url3 = "%srequest/?url=%s" % (requester, urllib.quote(url3, safe=''))

    print url1, requests.get(url1).content
    print url2, requests.get(url2).content
    print url3, requests.get(url3).content


def bing(sitemap_url, requester=False):
    url = "http://www.bing.com/webmaster/ping.aspx?siteMap=%s" % urlencode(sitemap_url)
    if requester:
       url = "%srequest/?url=%s" % (requester, urllib.quote(url, safe=''))
    print requests.get(url).content


def pingomatic(domain, requester=True):
    url = r"""http://pingomatic.com/ping/?rssurl=&chk_weblogscom=on&chk_blogs=on&chk_feedburner=on&chk_newsgator=on&chk_myyahoo=on&chk_pubsubcom=on&chk_blogdigger=on&chk_weblogalot=on&chk_newsisfree=on&chk_topicexchange=on&chk_google=on&chk_tailrank=on&chk_skygrid=on&chk_collecta=on&chk_superfeedr=on&chk_audioweblogs=on&chk_rubhub=on&chk_a2b=on&chk_blogshares=on"""

    url += "&title=%s&blogurl=%s" % (
        urllib.quote(domain, safe=''),
        urllib.quote("http://%s/" % domain, safe=''),
    )

    if requester:
       url = "%srequest/?url=%s" % (requester, urllib.quote(url, safe=''))

    print url, requests.get(url, headers={
        'Referer': "http://pingomatic.com/",
        'User-Agent': 'Mozilla/5.0 (Windows; U; MSIE 9.0; Windows NT 9.0; en-US)',
    }).status_code


def ping_se_domain(domain, promote_urls_titles, requesters):
    if requests.get('https://appengine.google.com').status_code == 403:
        requester_cr = random.choice(requesters)
    else:
        requester_cr = False
    if requests.get('https://ya.ru/', allow_redirects=False).status_code != 200:
        requester_ua = random.choice(requesters)
    else:
        requester_ua = False

    try:
        sitemap_url = "http://%s/sitemap.xml" % domain
        google(sitemap_url, requester=requester_cr)
        bing(sitemap_url, requester=requester_cr)
        pingomatic(domain, requester=random.choice(requesters))

        #print 'promote_urls_titles:', promote_urls_titles
        #for url, title in promote_urls_titles:
        #    yandex(title, url, requester=requester_ua)
        #yandex('sitemap', sitemap_url, requester=requester_ua)
    except Exception as e:
        print e
        return False
