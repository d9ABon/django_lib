from settings import PROJECT_PATH
import logging, os

def get_logger(log_file):
    if os.pathsep not in log_file:
        log_file = os.path.join(PROJECT_PATH, 'logs', log_file)

    log_dir = os.path.dirname(log_file)
    if not os.path.isdir(log_dir):
        os.makedirs(log_dir)


    logger_ident = log_file #check this
    logger = logging.getLogger(logger_ident)



    hdlr = logging.FileHandler(log_file)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)

    # ensure we havent already registered the handler
    if logging.FileHandler not in map(lambda x: x.__class__, logger.handlers):
        logger.addHandler(hdlr)



    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(message)s') #%(name)-12s: %(levelname)-8s 
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger

    if logging.StreamHandler not in map(lambda x: x.__class__, logger.handlers):
        logger.addHandler(console)



    logger.setLevel(logging.INFO)

    return logger
