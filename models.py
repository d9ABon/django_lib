from django.db import models

from termcolor import colored

"""
import simplejson as json
from django.conf import settings
from django.db.models.query import QuerySet
"""

class BaseModel(models.Model):

    def __repr__(self):
        COLORS = ['red', 'green', 'yellow', 'blue', 'magenta', 'cyan', 'white']
        return '<%s: %s>' % (self.__class__.__name__, colored(self.__unicode__(), COLORS[hash(self.__unicode__()) % len(COLORS)]))
    def html_repr(self):
        COLORS = ['Aqua','Black','BlueViolet','Chartreuse','Coral','Crimson','DarkBlue','DarkGoldenRod',
                  'DarkGreen','DarkMagenta','Darkorange','DarkRed','DarkSeaGreen','DarkSlateGray','DarkViolet','DeepSkyBlue',
                  'DimGrey','FireBrick','Fuchsia','GoldenRod','Green','HotPink','Indigo','LawnGreen','LightCoral',
                  'LightSalmon','LightSkyBlue','LightSteelBlue','LimeGreen','Maroon','MediumBlue','MediumPurple',
                  'MediumSlateBlue','MediumTurquoise','MidnightBlue','Olive','Orange','Orchid','PaleVioletRed',
                  'Red','RoyalBlue','Salmon','SeaGreen','SlateBlue',
                  'Turquoise','YellowGreen',]
        return '<span style="color:%(color)s;font-weight: bolder;" data-kw-id="%(id)d">%(repr)s</span>' % {
            'color': COLORS[self.id % len(COLORS)].lower(),
            'repr': self.__unicode__(),
            'id': self.id,
        }

    def __str__(self):
        fields = self.__class__._meta.get_all_field_names()
        if 'title' in fields:
            ret = self.title
        elif 'name' in fields:
            ret = self.name
        else:
            ret = "id: %d" % self.id

        return ret[:20]

    def __unicode__(self):
        ret = self.__str__()
        return ret

    class Meta:
        abstract = True










"""
class SerializedObjectField(models.TextField):
    # Model field that stores serialized value of model class instance
    # and returns deserialized model instance
    #
    # from django.db import models
    # import SerializedObjectField
    #
    # class A(models.Model):
    #    object = SerializedObjectField()
    #
    # class B(models.Model):
    #    field = models.CharField(max_length=10)
    # b = B(field='test')
    # b.save()
    # a = A()
    # a.object = b
    # a.save()
    # a = A.object.get(pk=1)
    # a.object
    # <B: B object>
    # a.object.__dict__
    # {'field': 'test', 'id': 1}

    def _serialize(self, value):
        if not value:
            return ''

        if not isinstance(value, QuerySet):
            value = [value]

        return json.dumps(value)

    def _deserialize(self, value):
        objs = [obj for obj in json.loads(value.encode(settings.DEFAULT_CHARSET))]

        if len(objs) == 1:
            return objs[0].object
        else:
            return [obj.object for obj in objs]

    def db_type(self, connection):
        return 'text'

    def pre_save(self, model_instance, add):
        value = getattr(model_instance, self.attname, None)
        return self._serialize(value)

    def contribute_to_class(self, cls, name, virtual_only=False):
        self.class_name = cls
        super(SerializedObjectField, self).contribute_to_class(cls, name)
        models.signals.post_init.connect(self.post_init)

    def post_init(self, **kwargs):
        if 'sender' in kwargs and 'instance' in kwargs:
            if kwargs['sender'] == self.class_name and \
            hasattr(kwargs['instance'], self.attname):
                value = self.value_from_object(kwargs['instance'])

                if value:
                    setattr(kwargs['instance'], self.attname,
                            self._deserialize(value))
                else:
                    setattr(kwargs['instance'], self.attname, None)
"""