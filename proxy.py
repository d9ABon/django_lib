#!/usr/bin/env python
# -*- coding: utf8 -*-
import Queue
import threading
import re
import os, sys
import random
import time
import datetime
from math import log10
from cookielib import LWPCookieJar, CookieJar, Cookie

import urllib
import urllib2

import requests
import requests.packages.urllib3
try:
    requests.packages.urllib3.disable_warnings()
except Exception:
    pass

from urlparse import urlparse

from settings import PROJECT_PATH, global_settings

if __name__ == "__main__":
    PROJECT_PATH = os.path.abspath(os.path.dirname(os.path.dirname(__file__))).replace('\\', '/')
    sys.path.insert(0, PROJECT_PATH)
    sys.path.append(os.path.dirname(PROJECT_PATH)) #parent
    # import settings as default_settings
    from django.conf import settings
    settings.configure()


from lib.base import *
from lib.decorators import retry_on_exception, profile_fun, time_limit, TimeoutException


try:
    from django.core.cache import cache
except ImportError:
    print 'using dummy cache!'
    cache = Dummy(
        get=lambda x: None,
        add=lambda x,y,z: None,
        set=lambda x,y,z: None,
    )


threads = 30
queue = Queue.Queue()
output = []









def choose_requester_app():
    def time_in_range(start, end, x):
        """Return true if x is in the range [start, end]"""
        if start <= end:
            return start <= x <= end
        else:
            return start <= x or x <= end

    current = datetime.datetime.utcnow().time()

    avaliable_time_ranges = global_settings()['requester_apps'].keys()
    random.shuffle(avaliable_time_ranges)
    for time_range in avaliable_time_ranges:
        start = datetime.time(*map(int, time_range.split('-')[0].split(':')))
        end = datetime.time(*map(int, time_range.split('-')[1].split(':')))
        if time_in_range(start, end, current):
            return random.choice(global_settings()['requester_apps'][time_range])
    raise Exception('no available apps for current time')












class Proxy(object):

    proxies = []
    current_proxy = None


    @retry_on_exception(Exception, retries=10)
    def get_proxies(self):
        proxies = cache.get('proxies')
        if proxies:
            self.proxies = proxies
            random.shuffle(self.proxies)
            return self.proxies

        url = 'https://proxylistio.appspot.com/?limit=50'
        url = choose_requester_app()+'request/?%s' % urllib.urlencode({'url': url})

        proxies = requests.get(url, timeout=20).text

        self.proxies = proxies.split("\n")

        if not re.match('[\d:\.]+', self.proxies[0]):
            raise Exception("don't look like proxy "+str(self.proxies[0])+' '+url)

        random.shuffle(self.proxies)
        cache.set('proxies', self.proxies, 60)


    def get_proxy_working_with_page(self, url, find_in_page=None, timeout=12):
        while True:
            if not len(self.proxies):
                self.get_proxies()

            self.current_proxy = self.proxies[0]
            print 'using proxy %s' % self.current_proxy

            try:

                result = self._request(url, proxy=self.current_proxy, timeout=timeout)
                if find_in_page and find_in_page not in result:
                    raise Exception('%s not in page' % find_in_page)
                else:
                    return self.current_proxy
            except Exception as e:
                print e
                self.proxies = self.proxies[1:]
                continue




    def request(self, url, post_data=None, next_proxy_on=None, timeout=12):
        while True:
            if not len(self.proxies):
                self.get_proxies()

            self.current_proxy = self.proxies[0]
            print 'using proxy %s' % self.current_proxy

            try:
                with time_limit(timeout):
                    result = self._request(url, proxy=self.current_proxy, post_data=post_data, timeout=timeout)
                if next_proxy_on:
                    for s in next_proxy_on:
                        if s in result:
                            raise Exception('trying next proxy')
                return result
            except TimeoutException:
                print 'timeout. trying next proxy'
                self.proxies = self.proxies[1:]
                continue
            except Exception as e:
                print e
                self.proxies = self.proxies[1:]
                continue


    def _request(self, url, proxy, post_data=None, timeout=12):
        parsed_url = urlparse(url)
        headers = {
            'User-Agent': random.choice(global_settings()['useragents']),
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Language': 'ru,en-US;q=0.8,en;q=0.6,uk;q=0.4',
            'Connection': 'keep-alive',
            'Dnt': '1',
            'Referer': "%s://%s" % (parsed_url.scheme, parsed_url.netloc),
            'Upgrade-Insecure-Requests': '1',
            'X-Compress': '1',
            'Accept-Encoding': 'gzip, deflate, sdch',
            'X-JAVASCRIPT-ENABLED': 'false',
        }

        if post_data:
            resp = requests.post(url, post_data, proxies={"http":"http://%s" % proxy, "https":"http://%s" % proxy}, timeout=timeout, headers=headers).text
        else:
            resp = requests.get(url, proxies={"http":"http://%s" % proxy, "https":"http://%s" % proxy}, timeout=timeout, headers=headers).text

        if not len(resp.strip()):
            raise Exception('zero length')

        #import ipdb;ipdb.set_trace()
        return resp






class ThreadUrl(threading.Thread):
    """Threaded Url Grab"""
    def __init__(self, queue):
        threading.Thread.__init__(self)
        self.queue = queue

    def run(self):
        while True:
            #grabs host from queue
            proxy_info = self.queue.get()

            try:
                proxy_handler = urllib2.ProxyHandler({'http': proxy_info})
                opener = urllib2.build_opener(proxy_handler)
                opener.addheaders = [('User-agent', 'Mozilla/5.0')]

                response = opener.open("http://www.google.com", timeout=7)
                rs = response.read()
                # urllib2.install_opener(opener)
                # req = urllib2.Request("http://www.google.com")
                # sock=urllib2.urlopen(req, timeout=7)
                # rs = sock.read(1000)
                if '<title>Google</title>' in rs:
                    output.append((True, proxy_info))
                else:
                    raise Exception("Not Google")
            except:
                output.append((False, proxy_info))
            #signals to queue job is done
            self.queue.task_done()


class Checker(object):

    @staticmethod
    def check(hosts):

        #spawn a pool of threads, and pass them queue instance
        for i in range(5):
            t = ThreadUrl(queue)
            t.setDaemon(True)
            t.start()
        #populate queue with data
        for host in hosts:
            queue.put(host)

        #wait on the queue until everything has been processed
        queue.join()


    @staticmethod
    def get_list():
        from pyquery import PyQuery

        def request(url):
            opener = urllib2.build_opener()
            opener.addheaders = [('User-agent', 'Mozilla/5.0 (Windows; U; MSIE 9.0; Windows NT 9.0; en-US)')]
            response = opener.open(url)
            resp = response.read()
            return resp

        ip_re_part = '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}'
        port_re_part = '\d{1,5}'
        pr_re1 = re.compile('[^\d](%s:%s)[^\d]' % (ip_re_part, port_re_part))

        proxies = []


        def fineproxy():
            html = request('http://fineproxy.org/свежие-прокси/')
            return pr_re1.findall(html)


        """
        #deprecated
        href_re1 = re.compile('<a href="(proxylist_[^"]+html)"')
        pr_re2 = re.compile('[^\d](%s)</span>(%s)</div>[^\d]' % (ip_re_part, port_re_part))
        html = request('http://www.checkedproxylists.com/')
        for relative_url in href_re1.findall(html):
            xml = request('http://www.checkedproxylists.com/load_%s' % relative_url)
            xml = xml.replace('&lt;', '<').replace('&gt;', '>')
            proxies += ["%s:%s" % group for group in pr_re2.findall(xml)]
        """


        def foxtools():
            proxies = []
            pr_re3 = re.compile('[^\d](%s)\s*</td>\s*<td[^>]*>(%s)[^\d]' % (ip_re_part, port_re_part), re.MULTILINE)
            for page in range(1, 10):
                html = request('http://foxtools.ru/Proxy?page=%d' % page)
                res = pr_re3.findall(html)
                proxies += map(lambda x: ":".join(x), res)
            return proxies




        def xseo():
            html = request('http://xseo.in/freeproxy')
            return pr_re1.findall(html)



        def hidemyass():
            from bs4 import BeautifulSoup
            proxies = []

            for page in range(1,5):
                html = request('http://proxylist.hidemyass.com/%s' % page)

                bs = BeautifulSoup(html, "lxml")
                tbody = bs.find('table', {'id':'listable'}).find('tbody')
                trs = tbody.findAll('tr')

                for tr in trs:
                    ip_td = tr.findAll('td')[1]
                    ip_td_html = str(ip_td)

                    no_disp = re.findall(r'\.(.+)\{display:none\}', ip_td.style.string)


                    def find_visible(tag):
                        cls = tag.get('class', '')
                        cls = cls[0] if cls else '---'
                        return (not tag.name == 'style') \
                                   and (cls not in no_disp ) \
                                   and ('display:none' not in tag.get('style', ''))
                    def find_hidden(tag):
                        cls = tag.get('class', '')
                        cls = cls[0] if cls else '---'
                        return (tag.name == 'style') \
                                   or (cls in no_disp ) \
                                   or ('display:none' in tag.get('style', ''))



                    for tag in ip_td.find_all(find_hidden, text=True):
                        #tag.string
                        ip_td_html = ip_td_html.replace(str(tag), '')

                    ip = re.sub('<[^>]*>', '', ip_td_html, flags=re.MULTILINE | re.DOTALL)
                    port = tr.findAll('td')[2].text.strip()

                    proxies += [ip+':'+port]

            return proxies


        proxies += hidemyass()
        proxies += fineproxy()
        proxies += foxtools()
        proxies += xseo()

        return proxies


if __name__ == "__main__":
    #start = time.time()
    to_check = Checker.get_list()
    Checker.check(to_check)

    for working, host in output:
        if working:
            print host

    #print "Elapsed Time: %s" % (time.time() - start)