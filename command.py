from __future__ import print_function
import inspect
import sys
from django.core.management.base import BaseCommand

from .base import termtitle

class Command(BaseCommand):
    parser = None
    commands = None

    def get_argspec(self, command):
        argspec = inspect.getargspec(command)
        argspec = {
            'args': argspec.args if argspec.args else [],
            'defaults': argspec.defaults if argspec.defaults else [],
        }
        required_args_len = len(argspec['args']) - len(argspec['defaults'])
        argspec['defaults_dict'] = dict(zip(argspec['args'][required_args_len:], argspec['defaults']))
        return argspec, required_args_len

    def add_arguments(self, parser):
        self.parser = parser

        #remove default actions
        # from argparse import Action
        # for action in parser._actions[:]:
        #     if isinstance(action, Action):
        #         del parser._actions[parser._actions.index(action)]


        self.commands = []
        for name, obj in inspect.getmembers(self.__class__.command_module):
            if not inspect.isfunction(obj):
                continue
            if inspect.getmodule(obj) != inspect.getmodule(self.__class__.command_module):
                continue
            if name[0] == '_':
                continue
            self.commands += [(name, obj)]
        self.commands = dict(self.commands)

        if len(sys.argv) > 2 and sys.argv[2][1:] in self.commands.keys():
            command_name = sys.argv[2][1:]
            parser.add_argument('-'+command_name, action="store_const", const=command_name, dest="command_name")
            command = self.commands[command_name]
            argspec, required_args_len = self.get_argspec(command)
            for required in argspec['args'][:required_args_len]:
                parser.add_argument('--'+required, metavar="<%s>" % required)
            for i, optional in enumerate(argspec['args'][required_args_len:]):
                metavar = argspec['defaults'][i] if argspec['defaults'][i] is not None else 'None'
                parser.add_argument('--'+optional, default=argspec['defaults'][i], metavar=metavar)
        else:
            for command_name in self.commands.keys():
                parser.add_argument('-'+command_name, metavar='', action="store_const", const=command_name, dest="command_name")
            parser.error('choose command')


    def handle(self, *args, **options):
        options.pop('settings')
        options.pop('pythonpath')
        options.pop('verbosity')
        options.pop('traceback')
        options.pop('no_color')
        options.pop('force_color')
        command_name = options.pop('command_name')
        argspec, required_args_len = self.get_argspec(self.commands[command_name])

        for key, val in options.items():
            if val is None and (key not in argspec['defaults_dict'].keys() or argspec['defaults_dict'][key] is not None):
                self.parser.error('--%s option is required' % key)
            if not val:
                continue
            if not isinstance(val, int):
                if sys.version_info[0] < 3 and not isinstance(val, unicode):  #python3
                    options[key] = val.decode('utf-8')
                if options[key] and options[key].isnumeric():
                    options[key] = int(options[key])

        #termtitle('%s - %s' % (self.__class__.command_module.__name__.split('.')[-1], command_name))



        ret = self.commands[command_name](**options)

        if ret is not None:
            print(ret)