# -*- coding: utf-8 -*-
from django import template
from django.conf import settings



register = template.Library()

"""
@register.tag()
def img_url(parser, token):
    try:
        # метод split_contents() знает, что не надо разделять строки в кавычках
        opts = token.split_contents()
        if len(opts) == 4:
            tag_name, instance_name, field, version = token.split_contents()
        else:
            tag_name, instance_name, field = token.split_contents()
            version = 'main'
    except ValueError:
        # камрады с мест сообщают, что в следующей строке должно использоваться
        # token.split_contents()[0], требуется дополнительное подтверждение.
        msg = '%r tag required arguments: object instance name, field. Optional: image resize version' % token.contents[0]
        raise template.TemplateSyntaxError(msg)
    return ImgUrl(instance_name, field, version)#format_string[1:-1]

class ImgUrl(template.Node):

    def __init__(self, instance_name, field, version='main'):
        self.instance_name = instance_name
        self.field = field
        self.version = version

    def render(self, context):
        obj = context[self.instance_name]

        if self.version != 'main':
            return ''.join([settings.MEDIA_URL+obj.get_upload_to(self.field), '/', str(obj.id), '_', self.version, '_', getattr(obj, self.field).name])
        else:
            return ''.join([settings.MEDIA_URL+obj.get_upload_to(self.field), '/', str(obj.id), '_', getattr(obj, self.field).name])
"""



@register.tag()
def join_passed_args(parser, token):
    class JoinPassedArgsParser(template.TokenParser):
        def top(self):
            passed_args = [self.value().split(':')]
            while self.more():
                passed_args += [self.value().split(':')]
            return dict(passed_args)

    passed_args = JoinPassedArgsParser(token.contents).top()

    return JoinPassedArgs(passed_args)

class JoinPassedArgs(template.Node):

    def __init__(self, passed_args):
        #{key:value,...} become {key:Variable(value),...)
        self.passed_args = dict(map(lambda (key, value): [key, template.Variable(value)], passed_args.items()))

    def render(self, context):
        #{key:value,...} become {key:value.resolve(context),...}
        passed_args = dict(map(lambda (key, value): [key, value.resolve(context)], self.passed_args.items()))

        context_passed_args = context['passed_args']
        context_passed_args.update(passed_args)

        #{a:b,c:d} become 'a:b/c:d'
        urlpart = '/'.join(map(lambda (x, y): ':'.join([x, str(y)]), context_passed_args.items()))
        return urlpart
