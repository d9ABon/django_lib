from django import template
import itertools
import datetime
import re
import json
import random
from lib.base import chunks, sentences
register = template.Library()

from django.utils.translation import ngettext_lazy


@register.filter
def humanize_time_diff(timestamp):
    """
    Returns a humanized string representing time difference
    between now() and the input timestamp.

    The output rounds up to days, hours, minutes, or seconds.
    4 days 5 hours returns '4 days'
    0 days 4 hours 3 minutes returns '4 hours', etc...
    """
    timeDiff = datetime.datetime.now() - timestamp
    days = timeDiff.days
    hours = timeDiff.seconds/3600
    minutes = timeDiff.seconds%3600/60
    seconds = timeDiff.seconds%3600%60

    if days > 0:
        str = ngettext_lazy("%s day ago", "%s days ago", days) % days
    elif hours > 0:
        str = ngettext_lazy("%s hour ago", "%s hours ago", hours) % hours
    elif minutes > 0:
        str = ngettext_lazy("%s minute ago", "%s minutes ago", minutes) % minutes
    elif seconds > 0:
        str = ngettext_lazy("%s second ago", "%s seconds ago", seconds) % seconds
    else:
       str = ''

    return str


@register.filter
def ucfirst(strng):
    if not strng:
        return ''
    return strng[0].upper() + strng[1:].lower()


@register.filter
def to_json(obj):
    return json.dumps(obj)


@register.filter
def sorted_list(lst, cmp=None, key=None, reverse=False):
    return sorted(lst, cmp=cmp, key=key, reverse=reverse)


"""
usage: {% make_list var1 var2 var3 as some_list %}
"""
@register.tag
def make_list(parser, token):
  bits = list(token.split_contents())
  if len(bits) >= 4 and bits[-2] == "as":
    varname = bits[-1]
    items = bits[1:-2]
    return MakeListNode(items, varname)
  else:
    raise template.TemplateSyntaxError("%r expected format is 'item [item ...] as varname'" % bits[0])

class MakeListNode(template.Node):
  def __init__(self, items, varname):
    self.items = map(template.Variable, items)
    self.varname = varname

  def render(self, context):
    context[self.varname] = [ i.resolve(context) for i in self.items ]
    return ""


class RandomNode(template.Node):
    def __init__(self, nodelist_options):
        self.nodelist_options = nodelist_options
    def render(self, context):
        return random.choice(self.nodelist_options).render(context)
@register.tag
def random_block(parser, token):
    """
    Output the contents of a random block.

    The `random` block tag must contain one or more `or` tags, which separate
    possible choices; a choice in this context is everything between a
    `random` and `or` tag, between two `or` tags, or between an `or` and an
    `endrandom` tag.

    Sample usage::

        {% random_block %}
        You will see me half the time.
        {% or %}
        You will see <em>me</em> the other half.
        {% endrandom %}
    """
    options = template.NodeList()
    while True:
        option = parser.parse(('or', 'endrandom'))
        token = parser.next_token()
        options.append(option)
        if token.contents == 'or':
            continue
        parser.delete_first_token()
        break
    if len(options) < 2:
        raise template.TemplateSyntaxError, "random must have at least two possibilities"
    return RandomNode(options)


register.filter('chunks', chunks)  #from lib.base
register.filter('sentences', sentences)  #from lib.base