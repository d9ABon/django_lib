# -*- coding: utf-8 -*-
from django import template

from lib.base import UrlManipulate

register = template.Library()

LEADING_PAGE_RANGE_DISPLAYED = TRAILING_PAGE_RANGE_DISPLAYED = 5
LEADING_PAGE_RANGE = TRAILING_PAGE_RANGE = 4
NUM_PAGES_OUTSIDE_RANGE = 1
ADJACENT_PAGES = 4

def paginator(context):
    paginated = context['paginated_items']
    num_pages = paginated.paginator.num_pages

    #" Initialize variables "
    in_leading_range = in_trailing_range = False
    pages_outside_leading_range = pages_outside_trailing_range = range(0)

    if num_pages <= LEADING_PAGE_RANGE_DISPLAYED:
        in_leading_range = in_trailing_range = True
        page_numbers = [n for n in range(1, num_pages + 1) if n > 0 and n <= num_pages]
    elif paginated.number <= LEADING_PAGE_RANGE:
        in_leading_range = True
        page_numbers = [n for n in range(1, LEADING_PAGE_RANGE_DISPLAYED + 1) if n > 0 and n <= num_pages]
        pages_outside_leading_range = [n + num_pages for n in range(0, -NUM_PAGES_OUTSIDE_RANGE, -1)]
    elif paginated.number > num_pages - TRAILING_PAGE_RANGE:
        in_trailing_range = True
        page_numbers = [n for n in range(num_pages - TRAILING_PAGE_RANGE_DISPLAYED + 1, num_pages + 1) if n > 0 and n <= num_pages]
        pages_outside_trailing_range = [n + 1 for n in range(0, NUM_PAGES_OUTSIDE_RANGE)]
    else:
        page_numbers = [n for n in range(paginated.number - ADJACENT_PAGES, paginated.number + ADJACENT_PAGES + 1) if n > 0 and n <= num_pages]
        pages_outside_leading_range = [n + num_pages for n in range(0, -NUM_PAGES_OUTSIDE_RANGE, -1)]
        pages_outside_trailing_range = [n + 1 for n in range(0, NUM_PAGES_OUTSIDE_RANGE)]
    ret = {
        "is_paginated": True,
        "has_previous": paginated.has_previous(),
        "has_next": paginated.has_next(),
        "results_per_page": paginated.paginator.per_page,
        "page": paginated.number,
        "pages": num_pages,
        "page_numbers": page_numbers,
        "in_leading_range" : in_leading_range,
        "in_trailing_range" : in_trailing_range,
        "pages_outside_leading_range": pages_outside_leading_range,
        "pages_outside_trailing_range": pages_outside_trailing_range,

        "paginated_items": context['paginated_items'],
    }
    if paginated.paginator.base_url:
        ret['base_url'] = paginated.paginator.base_url
    else:
        ret['request'] = context['request']
    if ret['has_previous']:
        ret['previous'] = paginated.previous_page_number()
    if ret['has_next']:
        ret['next'] = paginated.next_page_number()

#	if "sort" in context["named_args"]:
#		ret["sort"] = paginated.paginator.order_by
#	if "direction" in context["named_args"]:
#		ret["direction"] = paginated.paginator.direction
    return ret

"""
def side_list_paginator(context):
    paginated = context['paginated_items']

    ret = {
        "is_paginated": True,
        "previous": paginated.previous_page_number(),
        "has_previous": paginated.has_previous(),
        "next": paginated.next_page_number(),
        "has_next": paginated.has_next(),

        "paginated_items": context['paginated_items'],
    }
    if paginated.paginator.base_url:
        ret['base_url'] = paginated.paginator.base_url
    else:
        ret['request'] = context['request']

    return ret
register.inclusion_tag("templates/side_list_paginator.html", takes_context=True)(side_list_paginator)
"""

register.inclusion_tag("common/paginator.html", takes_context=True)(paginator)

@register.tag()
def paginator_order(parser, token):
    # метод split_contents() знает, что не надо разделять строки в кавычках
    try:
        tag_name, order_by, opts = token.split_contents()
        #opts are like: name=val;name2=val2
        #will be dict {name:val,name2:val2}
        opts = dict(map(lambda x: x.split('='), opts.split(';')))
    except ValueError: #opts are not required
        try:
            tag_name, order_by = token.split_contents()
            opts = {}
        except ValueError:
            # камрады с мест сообщают, что в следующей строке должно использоваться
            # token.split_contents()[0], требуется дополнительное подтверждение.
            msg = '%r tag required arguments: paginator_order' % token.contents[0]
            raise template.TemplateSyntaxError(msg)


    return PaginatorOrder(order_by[1:-1], opts)#format_string[1:-1]

class PaginatorOrder(template.Node):
    def __init__(self, order_by, opts):
        self.order_by = order_by
        self.opts = opts

    def render(self, context):
        paginated_items = context['paginated_items']

        if paginated_items.paginator.order_by == self.order_by \
                and paginated_items.paginator.direction == 'asc':
            dir = 'desc'
        else:
            dir = 'asc'

        url = paginated_items.paginator.base_url or context['request'].path
        url = UrlManipulate(url)
        url = url.set_named_param('sort', self.order_by)
        url = url.set_named_param('direction', dir)

        if 'remove_named' in self.opts:
            #list of named params to remove from url
            for named_param_name in self.opts['remove_named'].split(','):
                url = url.set_named_param(named_param_name, None)

        return url

#from urlparse import urlparse
#self.url = urlparse(context['request'].META['PATH_INFO'])[0:6]



@register.tag()
def set_named_args(parser, token):
    class SetNamedArgsParser(template.TokenParser):
        def top(self):
            named_args = [self.value().split('=')]
            while self.more():
                named_args += [self.value().split('=')]
            return dict(named_args)

    named_args = SetNamedArgsParser(token.contents).top()

    return SetNamedArgs(named_args)

class SetNamedArgs(template.Node):

    def __init__(self, named_args):
        #{key:value,...} become {key:Variable(value),...)
        self.named_args = dict(map(lambda (key, value): [key, template.Variable(value)], named_args.items()))

    def render(self, context):
        #{key:value,...} become {key:value.resolve(context),...}
        named_args = dict(map(lambda (key, value): [key, value.resolve(context)], self.named_args.items()))
        
        url = context['paginated_items'].paginator.base_url or context['request'].path

        #aaa = url[:]

        url = UrlManipulate(url)
        for key, val in named_args.items():
            url = url.set_named_param(key, val)

        #return "[%s],[%s],[%s],[%s]" % (aaa, url, context['paginated_items'].paginator.base_url, context['request'].path)
        return url
