from django.utils import text

register = template.Library()


@register.filter
def truncatehtml(string, length):
    """
    Truncate the text to a certain length, honoring html.

    Usage:

    {{ my_variable|truncatehtml:250 }}

    """

    return text.truncate_html_words(string, length)

truncatehtml.is_safe = True
