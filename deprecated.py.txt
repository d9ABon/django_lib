def update_videos_count_in_category_or_tag(sender, instance, action, reverse, model, pk_set, *args, **kwargs):
    """
    sender = Video.tags.through
    using	"default" (since the default router sends writes here)
    action = pre_add|post_add|pre_remove|post_remove|pre_clear|post_clear

    arguments if video.tags.add(tag):
    instance = video
    reverse = False (Video contains the ManyToManyField, so this call modifies the forward relation)
    model = Tag
    pk_set = [tag.id] (since only Tag was added to the relation)

    arguments if tag.video_set.remove(video):
    instance = tag
    reverse	= True (Video contains the ManyToManyField, so this call modifies the reverse relation)
    model = Video
    pk_set = [video.id]  (since only Video was removed from the relation)
    """

    if action not in ['post_add', 'post_remove', 'post_clear']:
        return True

    if not reverse:
        video = instance
        tag_or_category_ids = pk_set
        objects_list = [model.objects.get(id=id) for id in tag_or_category_ids]
    else:
        tag_or_category = instance
        video_ids = pk_set
        objects_list = [tag_or_category]

    for obj in objects_list:
        obj.videos_count = obj.get_videos_count()
        obj.save()

signals.m2m_changed.connect(update_videos_count_in_category_or_tag, sender=Video.tags.through)
signals.m2m_changed.connect(update_videos_count_in_category_or_tag, sender=Video.categories.through)
