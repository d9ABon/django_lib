# -*- coding: utf8 -*-
import os
import re
import random
import tempfile
import time
import subprocess
import shlex
from elasticsearch import Elasticsearch

from lib.base import pr
from lib.progress_meter import ProgressMeter

PROJECT_PATH = os.path.dirname(os.path.dirname(__file__))

from pymorphy import get_morph
morph = get_morph(os.path.join(PROJECT_PATH, 'pymorphy_dicts'))
with open(os.path.join(PROJECT_PATH, 'pymorphy_dicts', 'norm_words_blacklist.txt')) as f:
    blacklist = [l.strip().decode('utf-8').lower() for l in f.readlines() if len(l.strip())]


class NotFoundException(Exception):
    word_not_found = None
    def __init__(self, word_not_found):
        self.word_not_found = word_not_found


class FillText():
    text_file = None
    text_file_len = None
    queries = None
    recently_used_lines = None
    es = None
    index_name = None
    random_lines = []


    def __init__(self, queries, text_file):
        self.queries = queries
        self.text_file = text_file
        norm_file = 'normalized_%s' % os.path.basename(self.text_file)
        norm_file = os.path.join(os.path.dirname(self.text_file), norm_file)
        if not os.path.exists(norm_file):
            self.normalize_file(self.text_file, norm_file)


        self.es = Elasticsearch()
        self.index_name = os.path.basename(self.text_file).replace('.', '-')


        if not self.es.indices.exists(self.index_name):
            #es.indices.get_aliases().keys()
            #es.indices.delete(index='test-index', ignore=[400, 404])

            with open(norm_file) as f:
                text_data_lines = f.readlines()
            for i, line in enumerate(text_data_lines):
                try:
                    text_data_lines[i] = line.decode('utf-8')
                except UnicodeDecodeError:
                    del text_data_lines[i]

            text_data = {}  #format:  {'norm words': 'generated sentence.'}
            for l in text_data_lines:
                if not len(l.strip()):
                    continue
                l = l.strip().split("\t")
                if len(l) <= 1:
                    continue
                sent, norm = l[0], l[1]
                text_data[norm] = sent

            norm_text_data = []  #format:  [set(['norm', 'words']), set('...'), ...]
            for norm_words in text_data.keys():
                norm_text_data += [set(norm_words.split(';'))]

            for norm, sent in text_data.items():
                norm = norm.split(';')
                try:
                    self.es.index(index=self.index_name,
                                  doc_type='normalized_sentence_words',
                                  body={'sentence': sent, 'norm': norm}
                    )
                except Exception as e:
                    print e
                    continue
            del text_data
            del norm_text_data


        self.text_file_len = int(subprocess.Popen(["wc", "-l", self.text_file], stdout=subprocess.PIPE).communicate()[0].split(' ')[0])

        self.recently_used_lines = set()


    def split_to_words(self, s):
        return [w for w in re.split(r"""[.,;:!\?\(\)\s"'«»@#$%^&*=+_`~/\\<>]+""", s) if len(w)]


    def normalize_sentence(self, sentence):
        normalized = []
        for w in self.split_to_words(sentence):
            norm_w = morph.normalize(w.upper())
            if isinstance(norm_w, set):
                norm_w = norm_w.pop()
            if self.norm_word_is_bad(norm_w):
                continue
            normalized += [norm_w]
        normalized.sort()
        return normalized


    def normalize_file(self, text_file, norm_file):
        print 'start normalize file'
        with open(text_file) as f:
            sentences = []
            for l in f.readlines():
                try:
                    if len(l.strip()):
                        sentences += [l.strip().decode('utf-8')]
                except UnicodeDecodeError:
                    continue

        with open(norm_file, 'w+') as f:
            for sentence in sentences:
                f.write(sentence.replace("\t", '') + "\t" + ';'.join(self.normalize_sentence(sentence)) + "\n")
        print 'end normalize file'
        time.sleep(1)


    def norm_word_is_bad(self, norm_word):
        if len(norm_word) <= 3:
            return True
        if norm_word.lower() in blacklist:
            return True
        return False


    def search(self, norm_word):
        matches = self.es.search(index=self.index_name, q='norm:"%s"' % norm_word)
        hits = matches['hits']['hits']
        if not hits:
            raise NotFoundException(norm_word)

        random.shuffle(hits)

        for hit in hits:
            sentence = hit['_source']['sentence']
            if not isinstance(sentence, unicode):
                sentence = sentence.decode('utf-8')
            if sentence in self.recently_used_lines:
                continue

            self.recently_used_lines.add(sentence)
            #print norm_word
            #pr(sentence)
            return sentence

        raise NotFoundException(norm_word)


    def get_random_lines(self, num_lines):
        STACK_MIN_LEN = 500
        if len(self.random_lines) < STACK_MIN_LEN:
            random_lines = subprocess.Popen(["shuf", "-n %d" % STACK_MIN_LEN * 2, self.text_file],
                                            stdout=subprocess.PIPE).communicate()[0].split("\n")
            random_lines = [l.decode('utf-8') for l in random_lines if len(l.strip())]
            self.random_lines += random_lines

        #line_from = random.randint(1, self.text_file_len - num_lines + 1)
        #cmd = "awk 'NR>%d && NR<=%d' %s" % (line_from, line_from + num_lines, self.text_file)
        #random_lines = subprocess.Popen(shlex.split(cmd), stdout=subprocess.PIPE).communicate()[0].split("\n")

        random_lines = self.random_lines[:num_lines]
        self.random_lines = self.random_lines[num_lines:]
        return random_lines


    def combine_text_for_query(self, query, num_matched_sents, num_random_sents):
        ret = {
            'matched': [],
            'random': [],
        }

        norm_query_words_list = self.normalize_sentence(query)

        while len(ret['matched']) < num_matched_sents and len(norm_query_words_list):
            try:
                for norm_word in norm_query_words_list:
                    sentence = self.search(norm_word)
                    ret['matched'] += [sentence]
            except NotFoundException as e:
                #no more searching for this word
                del norm_query_words_list[norm_query_words_list.index(e.word_not_found)]


        if not len(ret['matched']):
            num_random_sents += num_matched_sents


        if len(ret['random']) < num_random_sents:
            random_lines = self.get_random_lines(num_random_sents - len(ret['random']))
            for sentence in random_lines:
                if sentence not in self.recently_used_lines:
                    ret['random'] += [sentence]
                    self.recently_used_lines.add(sentence)



        ret = ret['random'] + ret['matched']

        if len(self.recently_used_lines) > 50000:
            #print 'len(recently_used_lines)', len(self.recently_used_lines)
            for i in range(len(self.recently_used_lines) - 50000):
                self.recently_used_lines.pop()

        # freq_lst = frequency(query, words=normalize_to_words(ret))
        # pr(ret)
        # pr(freq_lst)
        # import ipdb;ipdb.set_trace()
        return ret


    def fill_text(self, num_matched_sents=12, num_random_sents=0, progress=False):
        pr = ProgressMeter(total=len(self.queries), rate_refresh=5) if progress else None

        combined_text_for_queries = {}
        for i, query in enumerate(self.queries):
            combined_text_for_queries[query] = self.combine_text_for_query(query, num_matched_sents, num_random_sents)
            if progress:
                pr.update(1)

        return combined_text_for_queries
        #most_common = Counter(words).most_common(1000)


#deprecated
"""
import ipdb;ipdb.set_trace()

for words in self.norm_text_data:
    if norm_word in words:
        del self.norm_text_data[self.norm_text_data.index(words)]

        #since it's a set()
        sentence = list(words)
        sentence.sort()
        sentence = ' '.join(sentence)
        return sentence
raise NotFoundException(norm_word)






if search_base_max_lines:
    os.system('shuf --head-count=%d --output=%s %s' % (search_base_max_lines, tmpfile, norm_file))
else:
    os.system('cp %s %s' % (norm_file, tmpfile))
with open(tmpfile) as f:
    text_data_lines = f.readlines()

"""