from functools import wraps, partial

from django.contrib.auth.decorators import user_passes_test#,permission_required, login_required,

#from django.core.exceptions import PermissionDenied
from django.contrib.auth import REDIRECT_FIELD_NAME

from django.shortcuts import get_object_or_404

from django.conf import settings

from django.core.exceptions import AppRegistryNotReady

from django.http import HttpResponseRedirect, HttpResponseForbidden
from django.utils.http import urlquote

from django.views.decorators.cache import cache_page as django_cache_page


def deny_bots(func=None, bot_names=None):
    # If called without method, we've been called with optional arguments.
    # We return a decorator with the optional arguments filled in.
    # Next time round we'll be decorating method.
    if func is None:
        return partial(deny_bots, bot_names=bot_names)

    if not bot_names:
        bot_names = ('bot', 'spider', 'crawl', 'slurp', 'twiceler')

    @wraps(func)
    def wrapper(request, *args, **kwargs):
        user_agent = request.META.get('HTTP_USER_AGENT', None)

        if not user_agent:
            return HttpResponseForbidden('address removed from crawling. check robots.txt')

        for bot in bot_names:
            if bot.lower() in user_agent.lower():
                return HttpResponseForbidden('address removed from crawling. check robots.txt')

        return func(request, *args, **kwargs)

    return wrapper



def superuser_required(function=None, login_url=None,
                                             redirect_field_name=REDIRECT_FIELD_NAME):
    actual_decorator = user_passes_test(
        lambda u: u.is_authenticated() and u.is_superuser,
        redirect_field_name=redirect_field_name
    )
    if function:
        return actual_decorator(function)
    return actual_decorator



"""
Usage:

@permission_required('blogs.change_entry')
@owner_required(Entry)
def manage_entry(request, object_id=None, object=None):

@permission_required('blogs.delete_entry')
@owner_required()
def entry_delete(*args, **kwargs):
    kwargs["post_delete_redirect"] = reverse('manage_blogs')
    return delete_object(*args, **kwargs)
"""
def owner_required(*args, **kwargs):
    kwargs['object_test'] = lambda obj: obj.user
    kwargs['user_test'] = lambda u: u
    return perm_object_and_user_based(*args, **kwargs)


def perm_object_and_user_based(object_test, user_test, model=None,
                                 kwarg_name='pk', login_url=None,
                                 redirect_field_name=REDIRECT_FIELD_NAME,
                                 replace_obj=False,allow_empty_pk=False):

    if not login_url:
        login_url = settings.LOGIN_URL

    def _decorator(viewfunc):
        def _wrapped_view(request, *args, **kwargs):
            user = request.user
            grant = False
            if kwarg_name not in kwargs:
                if not allow_empty_pk:
                    raise Exception('missing '+kwarg_name+' in kwargs')
                else:
                    #allow
                    return viewfunc(request, *args, **kwargs)

            object_id = int(kwargs[kwarg_name])

            object = get_object_or_404(model, pk=object_id)

            if user.is_superuser:
                grant = True
            else:
                if user.__class__ == model:
                    grant = object_id == user.id
                else:
                    """
                    # this selects all user's objects

                    names = [rel.get_accessor_name() for rel in user._meta.get_all_related_objects() if rel.model == model]
                    if names:
                        grant = object_id in [o.id for o in eval('user.%s.all()' % names[0])]
                    """
                    grant = object_test(object) == user_test(user)
            if not grant:

                path = urlquote(request.get_full_path())
                tup = login_url, redirect_field_name, path
                return HttpResponseRedirect('%s?%s=%s' % tup)

            if replace_obj:
                kwargs[kwarg_name] = object

            return viewfunc(request, *args, **kwargs)
        return wraps(viewfunc)(_wrapped_view)
    return _decorator

"""
def owner_required(test_func, model, kwarg_name='pk', redirect_field_name=REDIRECT_FIELD_NAME):
    @wraps(test_func)
    def view_decorator(view_function):
        @wraps(view_decorator)
        def decorated_view(request, *args, **kwargs):
            obj = get_object_or_404(model, pk=kwargs[kwarg_name])

            actual_decorator = user_passes_test(
                lambda u: (u.is_authenticated() and u.is_superuser) or u == obj.u,
                redirect_field_name=redirect_field_name
            )

            return actual_decorator(view_function)

        return decorated_view
    return view_decorator
"""


def cache_page(*args, **kwargs):
    key_prefix = kwargs.pop('key_prefix', None)
    key_prefix = "site_%d_%s" % (current_site.id, key_prefix)

    return django_cache_page(key_prefix=key_prefix, *args, **kwargs)

