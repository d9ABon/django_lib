import os

from django.utils import translation

from django.contrib.sites.models import get_current_site
try:
    current_site = get_current_site(None)
except Exception:
    from lib.base import Dummy
    current_site = Dummy(id=1, domain=os.environ['DOMAIN'])



from django import http

from lib.decorators import print_prof_data

class ViewNameMiddleware(object):
    def process_view(self, request, view_func, view_args, view_kwargs):
        if hasattr(view_func, '__name__'):
            request.view_name = ".".join((view_func.__module__, view_func.__name__))
        else:
            #feeds
            request.view_name = ".".join((view_func.__module__, view_func.__class__.__name__))


class LangInDomainMiddleware(object):
    #see transurlvania.middleware.LangInDomainMiddleware
    #was request.META['SERVER_NAME'] instead of request.META['HTTP_HOST'] in original middleware
    """
    Middleware for determining site's language via the domain name used in
    the request.
    This needs to be installed after the LocaleMiddleware so it can override
    that middleware's decisions.
    """
    def process_request(self, request):
        from transurlvania.settings import LANGUAGE_DOMAINS
        #from django.http import HttpResponsePermanentRedirect
        #HttpResponsePermanentRedirect('/nice-link')
        if 'HTTP_USER_AGENT' not in request.META:
            from django.http import Http404
            raise Http404('no HTTP_USER_AGENT')

        if 'yandex' in request.META['HTTP_USER_AGENT'].lower():
            translation.activate('ru')
            request.LANGUAGE_CODE = 'ru'
            return

        if 'HTTP_HOST' not in request.META: #strange request, seems like command-line
            translation.activate('ru')
            request.LANGUAGE_CODE = 'ru'
            return

        for lang in LANGUAGE_DOMAINS.keys():
            if LANGUAGE_DOMAINS[lang][0] == request.META['HTTP_HOST']:
                translation.activate(lang)
                request.LANGUAGE_CODE = translation.get_language()
                return


#redirect yandex to top domain
class PermRedirectMiddleware(object):
    def process_response(self, request, response):

#        import ipdb;ipdb.set_trace()

        if 'HTTP_USER_AGENT' not in request.META \
                or 'HTTP_HOST' not in request.META \
                or 'PATH_INFO' not in request.META:
                
            #probably CGI interface
            return response

        if 'robots.txt' in request.META['PATH_INFO']:
            #no redirect for robots.txt
            return response
                
        if 'yandex' not in request.META['HTTP_USER_AGENT'].lower():
            #not yandex - act as usual
            return response

        if request.META['HTTP_HOST'] == current_site.domain:
            #top domain - continue
            return response

        path = request.get_full_path()

        url = 'http://%(domain)s%(url)s' % {
            'domain': current_site.domain,
            'url': path,
        }

        return http.HttpResponsePermanentRedirect(url)


class ProfileFunMiddleware(object):
    def process_response(self, request, response):
        print_prof_data()
        return response


